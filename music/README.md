# Public Domain Music
The following songs were written before 1925 and are in the public domain. Arrangements by Nolan Baker. Composer and year listed below:

## Implemented

 - "Hurrah For Our National Game", Walter Neville, 1868
 - "Gee It's a Wonderful Game", G. Harris White, 1911
 - "Tessie (You Are the Only, Only, Only)", Will R. Anderson, 1902
 - "Take Me Out to the Ballgame", Albert Von Tilzer, 1908

## Planned
 - "Jake! Jake! The Yiddisher Ball Player", Irving Berlin, 1911
 - "Home Run Quick Step", John Zebley Jr., 1861
 - "The Base Ball March", Johann C. Schmid, 1905
 - "One-a-Strike" Arthur Longbrake, 1908
 - "That Baseball Rag", Clarence Jones, 1913

# Legally Grey
"Charge Fanfare", heard in the intro, borrows heavily from Tommy Walker's "Charge!" (1946) and the opening theme from Pokémon Red/Blue by Junichi Masuda. I'm fairly certain it counts as transformative or parody, but I could be really wrong.