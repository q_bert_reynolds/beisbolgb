
; File created by mod2gbt

    SECTION "intro_lights_0", ROMX
intro_lights_0:
    DB  $AF,$1F, $00, $00, $4A,$01
    DB  $AE,$91,$00, $00, $00, $00
    DB  $AD,$91,$00, $00, $00, $00
    DB  $AC,$91,$00, $00, $00, $00
    DB  $AB,$91,$00, $00, $00, $00
    DB  $AA,$91,$00, $00, $00, $00
    DB  $A9,$91,$00, $00, $00, $00
    DB  $A8,$91,$00, $00, $00, $00
    DB  $A7,$91,$00, $00, $00, $00
    DB  $A6,$91,$00, $00, $00, $00
    DB  $A5,$91,$00, $00, $00, $00
    DB  $A4,$91,$00, $00, $00, $00
    DB  $A3,$91,$00, $00, $00, $00
    DB  $A2,$91,$00, $00, $00, $00
    DB  $A1,$91,$00, $00, $00, $00
    DB  $A0,$91,$00, $00, $00, $00
    DB  $9F,$91,$00, $00, $00, $00
    DB  $9E,$91,$00, $00, $00, $00
    DB  $9D,$91,$00, $00, $00, $00
    DB  $9C,$91,$00, $00, $00, $00
    DB  $9B,$91,$00, $00, $00, $00
    DB  $9A,$91,$00, $00, $00, $00
    DB  $99,$91,$00, $00, $00, $00
    DB  $98,$91,$00, $00, $00, $00
    DB  $97,$91,$00, $00, $00, $00
    DB  $96,$91,$00, $00, $00, $00
    DB  $95,$91,$00, $00, $00, $00
    DB  $94,$91,$00, $00, $00, $00
    DB  $93,$91,$00, $00, $00, $00
    DB  $92,$91,$00, $00, $00, $00
    DB  $91,$91,$00, $00, $00, $00
    DB  $90,$91,$00, $00, $00, $00
    DB  $8F,$91,$00, $00, $00, $00
    DB  $8E,$91,$00, $00, $00, $00
    DB  $8D,$91,$00, $00, $00, $00
    DB  $8C,$91,$00, $00, $00, $2F
    DB  $98,$91,$37, $00, $00, $8B,$8A,$06
    DB  $2F, $00, $00, $2F
    DB  $2F, $00, $00, $2C
    DB  $2E, $00, $00, $28
    DB  $2D, $00, $00, $24
    DB  $2C, $00, $00, $20
    DB  $2B, $00, $00, $00
    DB  $2A, $00, $00, $00
    DB  $29, $00, $00, $00
    DB  $28, $00, $00, $00
    DB  $27, $00, $00, $00
    DB  $26, $00, $00, $00
    DB  $25, $00, $00, $00
    DB  $24, $00, $00, $00
    DB  $23, $00, $00, $00
    DB  $22, $00, $00, $00
    DB  $21, $00, $00, $00
    DB  $20, $00, $00, $00
    DB  $00, $00, $00, $00
    DB  $00, $00, $00, $00
    DB  $00, $00, $00, $00
    DB  $00, $00, $00, $00
    DB  $00, $00, $00, $00
    DB  $00, $00, $00, $00
    DB  $00, $00, $00, $00
    DB  $00, $00, $00, $00
    DB  $00, $00, $00, $00
    DB  $00, $00, $00, $00

  SECTION "intro_lights_data", ROMX
intro_lights_data::
    DB  BANK(intro_lights_0)
    DW  intro_lights_0
    DB  $00
    DW  $0000

