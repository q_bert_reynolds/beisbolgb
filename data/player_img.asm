DEF IMG_BANK_COUNT EQU 6
DEF PLAYERS_PER_BANK EQU (151 + (IMG_BANK_COUNT- 1)) / IMG_BANK_COUNT

SECTION "Player Images 0", ROMX, BANK[PLAYER_IMG_BANK+0]

DW PlayerTiles0
DW PlayerTileCounts0
DW PlayerColumns0
DW PlayerTileMaps0
INCLUDE "img/players/001Bubbi.asm"
INCLUDE "img/players/002Ivy.asm"
INCLUDE "img/players/003Venus.asm"
INCLUDE "img/players/004Ginger.asm"
INCLUDE "img/players/005Freckles.asm"
INCLUDE "img/players/006BigRed.asm"
INCLUDE "img/players/007Squirt.asm"
INCLUDE "img/players/008Pudge.asm"
INCLUDE "img/players/009Yogi.asm"
INCLUDE "img/players/010SweetPea.asm"
INCLUDE "img/players/011Meta.asm"
INCLUDE "img/players/012Butterfly.asm"
INCLUDE "img/players/013Weeds.asm"
INCLUDE "img/players/014KooKoo.asm"
INCLUDE "img/players/015Beedie.asm"
INCLUDE "img/players/016Pidge.asm"
INCLUDE "img/players/017Gio.asm"
INCLUDE "img/players/018OT.asm"
INCLUDE "img/players/019RatTail.asm"
INCLUDE "img/players/020Cait.asm"
INCLUDE "img/players/021Stoop.asm"
INCLUDE "img/players/022Crow.asm"
INCLUDE "img/players/023Snake.asm"
INCLUDE "img/players/024Cobra.asm"
INCLUDE "img/players/025Chu.asm"
INCLUDE "img/players/026Rai.asm"
INCLUDE "img/players/027Shrew.asm"

PlayerTiles0::
DW _001BubbiTiles
DW _002IvyTiles
DW _003VenusTiles
DW _004GingerTiles
DW _005FrecklesTiles
DW _006BigRedTiles
DW _007SquirtTiles
DW _008PudgeTiles
DW _009YogiTiles
DW _010SweetPeaTiles
DW _011MetaTiles
DW _012ButterflyTiles
DW _013WeedsTiles
DW _014KooKooTiles
DW _015BeedieTiles
DW _016PidgeTiles
DW _017GioTiles
DW _018OTTiles
DW _019RatTailTiles
DW _020CaitTiles
DW _021StoopTiles
DW _022CrowTiles
DW _023SnakeTiles
DW _024CobraTiles
DW _025ChuTiles
DW _026RaiTiles
DW _027ShrewTiles

PlayerTileCounts0::
DB _001BUBBI_TILE_COUNT
DB _002IVY_TILE_COUNT
DB _003VENUS_TILE_COUNT
DB _004GINGER_TILE_COUNT
DB _005FRECKLES_TILE_COUNT
DB _006BIGRED_TILE_COUNT
DB _007SQUIRT_TILE_COUNT
DB _008PUDGE_TILE_COUNT
DB _009YOGI_TILE_COUNT
DB _010SWEETPEA_TILE_COUNT
DB _011META_TILE_COUNT
DB _012BUTTERFLY_TILE_COUNT
DB _013WEEDS_TILE_COUNT
DB _014KOOKOO_TILE_COUNT
DB _015BEEDIE_TILE_COUNT
DB _016PIDGE_TILE_COUNT
DB _017GIO_TILE_COUNT
DB _018OT_TILE_COUNT
DB _019RATTAIL_TILE_COUNT
DB _020CAIT_TILE_COUNT
DB _021STOOP_TILE_COUNT
DB _022CROW_TILE_COUNT
DB _023SNAKE_TILE_COUNT
DB _024COBRA_TILE_COUNT
DB _025CHU_TILE_COUNT
DB _026RAI_TILE_COUNT
DB _027SHREW_TILE_COUNT

PlayerColumns0::
DB _001BUBBI_COLUMNS
DB _002IVY_COLUMNS
DB _003VENUS_COLUMNS
DB _004GINGER_COLUMNS
DB _005FRECKLES_COLUMNS
DB _006BIGRED_COLUMNS
DB _007SQUIRT_COLUMNS
DB _008PUDGE_COLUMNS
DB _009YOGI_COLUMNS
DB _010SWEETPEA_COLUMNS
DB _011META_COLUMNS
DB _012BUTTERFLY_COLUMNS
DB _013WEEDS_COLUMNS
DB _014KOOKOO_COLUMNS
DB _015BEEDIE_COLUMNS
DB _016PIDGE_COLUMNS
DB _017GIO_COLUMNS
DB _018OT_COLUMNS
DB _019RATTAIL_COLUMNS
DB _020CAIT_COLUMNS
DB _021STOOP_COLUMNS
DB _022CROW_COLUMNS
DB _023SNAKE_COLUMNS
DB _024COBRA_COLUMNS
DB _025CHU_COLUMNS
DB _026RAI_COLUMNS
DB _027SHREW_COLUMNS

PlayerTileMaps0::
DW _001BubbiTileMap
DW _002IvyTileMap
DW _003VenusTileMap
DW _004GingerTileMap
DW _005FrecklesTileMap
DW _006BigRedTileMap
DW _007SquirtTileMap
DW _008PudgeTileMap
DW _009YogiTileMap
DW _010SweetPeaTileMap
DW _011MetaTileMap
DW _012ButterflyTileMap
DW _013WeedsTileMap
DW _014KooKooTileMap
DW _015BeedieTileMap
DW _016PidgeTileMap
DW _017GioTileMap
DW _018OTTileMap
DW _019RatTailTileMap
DW _020CaitTileMap
DW _021StoopTileMap
DW _022CrowTileMap
DW _023SnakeTileMap
DW _024CobraTileMap
DW _025ChuTileMap
DW _026RaiTileMap
DW _027ShrewTileMap

SECTION "Player Images 1", ROMX, BANK[PLAYER_IMG_BANK+1]

DW PlayerTiles1
DW PlayerTileCounts1
DW PlayerColumns1
DW PlayerTileMaps1
INCLUDE "img/players/028Sandy.asm"
INCLUDE "img/players/029Chica.asm"
INCLUDE "img/players/030Muchacha.asm"
INCLUDE "img/players/031Reina.asm"
INCLUDE "img/players/032Chico.asm"
INCLUDE "img/players/033Muchacho.asm"
INCLUDE "img/players/034Ray.asm"
INCLUDE "img/players/035Fairy.asm"
INCLUDE "img/players/036Fable.asm"
INCLUDE "img/players/037Vulfpek.asm"
INCLUDE "img/players/038Foxy.asm"
INCLUDE "img/players/039Puff.asm"
INCLUDE "img/players/040Tuff.asm"
INCLUDE "img/players/041Bats.asm"
INCLUDE "img/players/042GoldBats.asm"
INCLUDE "img/players/043Oddie.asm"
INCLUDE "img/players/044Gloomy.asm"
INCLUDE "img/players/045Vile.asm"
INCLUDE "img/players/046Paris.asm"
INCLUDE "img/players/047Pariah.asm"
INCLUDE "img/players/048Natty.asm"
INCLUDE "img/players/049Mother.asm"
INCLUDE "img/players/050Diggs.asm"
INCLUDE "img/players/051Doug.asm"
INCLUDE "img/players/052Meow.asm"
INCLUDE "img/players/053Purr.asm"
INCLUDE "img/players/054Duck.asm"

PlayerTiles1::
DW _028SandyTiles
DW _029ChicaTiles
DW _030MuchachaTiles
DW _031ReinaTiles
DW _032ChicoTiles
DW _033MuchachoTiles
DW _034RayTiles
DW _035FairyTiles
DW _036FableTiles
DW _037VulfpekTiles
DW _038FoxyTiles
DW _039PuffTiles
DW _040TuffTiles
DW _041BatsTiles
DW _042GoldBatsTiles
DW _043OddieTiles
DW _044GloomyTiles
DW _045VileTiles
DW _046ParisTiles
DW _047PariahTiles
DW _048NattyTiles
DW _049MotherTiles
DW _050DiggsTiles
DW _051DougTiles
DW _052MeowTiles
DW _053PurrTiles
DW _054DuckTiles

PlayerTileCounts1::
DB _028SANDY_TILE_COUNT
DB _029CHICA_TILE_COUNT
DB _030MUCHACHA_TILE_COUNT
DB _031REINA_TILE_COUNT
DB _032CHICO_TILE_COUNT
DB _033MUCHACHO_TILE_COUNT
DB _034RAY_TILE_COUNT
DB _035FAIRY_TILE_COUNT
DB _036FABLE_TILE_COUNT
DB _037VULFPEK_TILE_COUNT
DB _038FOXY_TILE_COUNT
DB _039PUFF_TILE_COUNT
DB _040TUFF_TILE_COUNT
DB _041BATS_TILE_COUNT
DB _042GOLDBATS_TILE_COUNT
DB _043ODDIE_TILE_COUNT
DB _044GLOOMY_TILE_COUNT
DB _045VILE_TILE_COUNT
DB _046PARIS_TILE_COUNT
DB _047PARIAH_TILE_COUNT
DB _048NATTY_TILE_COUNT
DB _049MOTHER_TILE_COUNT
DB _050DIGGS_TILE_COUNT
DB _051DOUG_TILE_COUNT
DB _052MEOW_TILE_COUNT
DB _053PURR_TILE_COUNT
DB _054DUCK_TILE_COUNT

PlayerColumns1::
DB _028SANDY_COLUMNS
DB _029CHICA_COLUMNS
DB _030MUCHACHA_COLUMNS
DB _031REINA_COLUMNS
DB _032CHICO_COLUMNS
DB _033MUCHACHO_COLUMNS
DB _034RAY_COLUMNS
DB _035FAIRY_COLUMNS
DB _036FABLE_COLUMNS
DB _037VULFPEK_COLUMNS
DB _038FOXY_COLUMNS
DB _039PUFF_COLUMNS
DB _040TUFF_COLUMNS
DB _041BATS_COLUMNS
DB _042GOLDBATS_COLUMNS
DB _043ODDIE_COLUMNS
DB _044GLOOMY_COLUMNS
DB _045VILE_COLUMNS
DB _046PARIS_COLUMNS
DB _047PARIAH_COLUMNS
DB _048NATTY_COLUMNS
DB _049MOTHER_COLUMNS
DB _050DIGGS_COLUMNS
DB _051DOUG_COLUMNS
DB _052MEOW_COLUMNS
DB _053PURR_COLUMNS
DB _054DUCK_COLUMNS

PlayerTileMaps1::
DW _028SandyTileMap
DW _029ChicaTileMap
DW _030MuchachaTileMap
DW _031ReinaTileMap
DW _032ChicoTileMap
DW _033MuchachoTileMap
DW _034RayTileMap
DW _035FairyTileMap
DW _036FableTileMap
DW _037VulfpekTileMap
DW _038FoxyTileMap
DW _039PuffTileMap
DW _040TuffTileMap
DW _041BatsTileMap
DW _042GoldBatsTileMap
DW _043OddieTileMap
DW _044GloomyTileMap
DW _045VileTileMap
DW _046ParisTileMap
DW _047PariahTileMap
DW _048NattyTileMap
DW _049MotherTileMap
DW _050DiggsTileMap
DW _051DougTileMap
DW _052MeowTileMap
DW _053PurrTileMap
DW _054DuckTileMap

SECTION "Player Images 2", ROMX, BANK[PLAYER_IMG_BANK+2]

DW PlayerTiles2
DW PlayerTileCounts2
DW PlayerColumns2
DW PlayerTileMaps2
INCLUDE "img/players/055GoldDuck.asm"
INCLUDE "img/players/056Man.asm"
INCLUDE "img/players/057Primo.asm"
INCLUDE "img/players/058Lithium.asm"
INCLUDE "img/players/059Arcane.asm"
INCLUDE "img/players/060Polly.asm"
INCLUDE "img/players/061Swirl.asm"
INCLUDE "img/players/062Wrath.asm"
INCLUDE "img/players/063Bruh.asm"
INCLUDE "img/players/064Dab.asm"
INCLUDE "img/players/065Kazaa.asm"
INCLUDE "img/players/066Chip.asm"
INCLUDE "img/players/067Choke.asm"
INCLUDE "img/players/068Champ.asm"
INCLUDE "img/players/069Sprout.asm"
INCLUDE "img/players/070Weepy.asm"
INCLUDE "img/players/071Vic.asm"
INCLUDE "img/players/072Fonz.asm"
INCLUDE "img/players/073Cruel.asm"
INCLUDE "img/players/074Dude.asm"
INCLUDE "img/players/075Gravel.asm"
INCLUDE "img/players/076Golem.asm"
INCLUDE "img/players/077Pinto.asm"
INCLUDE "img/players/078Mustang.asm"
INCLUDE "img/players/079Slowpoke.asm"
INCLUDE "img/players/080Laggard.asm"
INCLUDE "img/players/081Magnet.asm"

PlayerTiles2::
DW _055GoldDuckTiles
DW _056ManTiles
DW _057PrimoTiles
DW _058LithiumTiles
DW _059ArcaneTiles
DW _060PollyTiles
DW _061SwirlTiles
DW _062WrathTiles
DW _063BruhTiles
DW _064DabTiles
DW _065KazaaTiles
DW _066ChipTiles
DW _067ChokeTiles
DW _068ChampTiles
DW _069SproutTiles
DW _070WeepyTiles
DW _071VicTiles
DW _072FonzTiles
DW _073CruelTiles
DW _074DudeTiles
DW _075GravelTiles
DW _076GolemTiles
DW _077PintoTiles
DW _078MustangTiles
DW _079SlowpokeTiles
DW _080LaggardTiles
DW _081MagnetTiles

PlayerTileCounts2::
DB _055GOLDDUCK_TILE_COUNT
DB _056MAN_TILE_COUNT
DB _057PRIMO_TILE_COUNT
DB _058LITHIUM_TILE_COUNT
DB _059ARCANE_TILE_COUNT
DB _060POLLY_TILE_COUNT
DB _061SWIRL_TILE_COUNT
DB _062WRATH_TILE_COUNT
DB _063BRUH_TILE_COUNT
DB _064DAB_TILE_COUNT
DB _065KAZAA_TILE_COUNT
DB _066CHIP_TILE_COUNT
DB _067CHOKE_TILE_COUNT
DB _068CHAMP_TILE_COUNT
DB _069SPROUT_TILE_COUNT
DB _070WEEPY_TILE_COUNT
DB _071VIC_TILE_COUNT
DB _072FONZ_TILE_COUNT
DB _073CRUEL_TILE_COUNT
DB _074DUDE_TILE_COUNT
DB _075GRAVEL_TILE_COUNT
DB _076GOLEM_TILE_COUNT
DB _077PINTO_TILE_COUNT
DB _078MUSTANG_TILE_COUNT
DB _079SLOWPOKE_TILE_COUNT
DB _080LAGGARD_TILE_COUNT
DB _081MAGNET_TILE_COUNT

PlayerColumns2::
DB _055GOLDDUCK_COLUMNS
DB _056MAN_COLUMNS
DB _057PRIMO_COLUMNS
DB _058LITHIUM_COLUMNS
DB _059ARCANE_COLUMNS
DB _060POLLY_COLUMNS
DB _061SWIRL_COLUMNS
DB _062WRATH_COLUMNS
DB _063BRUH_COLUMNS
DB _064DAB_COLUMNS
DB _065KAZAA_COLUMNS
DB _066CHIP_COLUMNS
DB _067CHOKE_COLUMNS
DB _068CHAMP_COLUMNS
DB _069SPROUT_COLUMNS
DB _070WEEPY_COLUMNS
DB _071VIC_COLUMNS
DB _072FONZ_COLUMNS
DB _073CRUEL_COLUMNS
DB _074DUDE_COLUMNS
DB _075GRAVEL_COLUMNS
DB _076GOLEM_COLUMNS
DB _077PINTO_COLUMNS
DB _078MUSTANG_COLUMNS
DB _079SLOWPOKE_COLUMNS
DB _080LAGGARD_COLUMNS
DB _081MAGNET_COLUMNS

PlayerTileMaps2::
DW _055GoldDuckTileMap
DW _056ManTileMap
DW _057PrimoTileMap
DW _058LithiumTileMap
DW _059ArcaneTileMap
DW _060PollyTileMap
DW _061SwirlTileMap
DW _062WrathTileMap
DW _063BruhTileMap
DW _064DabTileMap
DW _065KazaaTileMap
DW _066ChipTileMap
DW _067ChokeTileMap
DW _068ChampTileMap
DW _069SproutTileMap
DW _070WeepyTileMap
DW _071VicTileMap
DW _072FonzTileMap
DW _073CruelTileMap
DW _074DudeTileMap
DW _075GravelTileMap
DW _076GolemTileMap
DW _077PintoTileMap
DW _078MustangTileMap
DW _079SlowpokeTileMap
DW _080LaggardTileMap
DW _081MagnetTileMap

SECTION "Player Images 3", ROMX, BANK[PLAYER_IMG_BANK+3]

DW PlayerTiles3
DW PlayerTileCounts3
DW PlayerColumns3
DW PlayerTileMaps3
INCLUDE "img/players/082Ton.asm"
INCLUDE "img/players/083Dubious.asm"
INCLUDE "img/players/084Duce.asm"
INCLUDE "img/players/085Trey.asm"
INCLUDE "img/players/086Seal.asm"
INCLUDE "img/players/087Sealion.asm"
INCLUDE "img/players/088Greaser.asm"
INCLUDE "img/players/089Yuck.asm"
INCLUDE "img/players/090Cup.asm"
INCLUDE "img/players/091Dish.asm"
INCLUDE "img/players/092Gas.asm"
INCLUDE "img/players/093Morty.asm"
INCLUDE "img/players/094Macobb.asm"
INCLUDE "img/players/095Onyx.asm"
INCLUDE "img/players/096Zee.asm"
INCLUDE "img/players/097Medium.asm"
INCLUDE "img/players/098Dick.asm"
INCLUDE "img/players/099Rex.asm"
INCLUDE "img/players/100Speedball.asm"
INCLUDE "img/players/101Tucker.asm"
INCLUDE "img/players/102Ed.asm"
INCLUDE "img/players/103Edd.asm"
INCLUDE "img/players/104Bones.asm"
INCLUDE "img/players/105Marrow.asm"
INCLUDE "img/players/106Bruce.asm"
INCLUDE "img/players/107Jackie.asm"
INCLUDE "img/players/108Tongue.asm"

PlayerTiles3::
DW _082TonTiles
DW _083DubiousTiles
DW _084DuceTiles
DW _085TreyTiles
DW _086SealTiles
DW _087SealionTiles
DW _088GreaserTiles
DW _089YuckTiles
DW _090CupTiles
DW _091DishTiles
DW _092GasTiles
DW _093MortyTiles
DW _094MacobbTiles
DW _095OnyxTiles
DW _096ZeeTiles
DW _097MediumTiles
DW _098DickTiles
DW _099RexTiles
DW _100SpeedballTiles
DW _101TuckerTiles
DW _102EdTiles
DW _103EddTiles
DW _104BonesTiles
DW _105MarrowTiles
DW _106BruceTiles
DW _107JackieTiles
DW _108TongueTiles

PlayerTileCounts3::
DB _082TON_TILE_COUNT
DB _083DUBIOUS_TILE_COUNT
DB _084DUCE_TILE_COUNT
DB _085TREY_TILE_COUNT
DB _086SEAL_TILE_COUNT
DB _087SEALION_TILE_COUNT
DB _088GREASER_TILE_COUNT
DB _089YUCK_TILE_COUNT
DB _090CUP_TILE_COUNT
DB _091DISH_TILE_COUNT
DB _092GAS_TILE_COUNT
DB _093MORTY_TILE_COUNT
DB _094MACOBB_TILE_COUNT
DB _095ONYX_TILE_COUNT
DB _096ZEE_TILE_COUNT
DB _097MEDIUM_TILE_COUNT
DB _098DICK_TILE_COUNT
DB _099REX_TILE_COUNT
DB _100SPEEDBALL_TILE_COUNT
DB _101TUCKER_TILE_COUNT
DB _102ED_TILE_COUNT
DB _103EDD_TILE_COUNT
DB _104BONES_TILE_COUNT
DB _105MARROW_TILE_COUNT
DB _106BRUCE_TILE_COUNT
DB _107JACKIE_TILE_COUNT
DB _108TONGUE_TILE_COUNT

PlayerColumns3::
DB _082TON_COLUMNS
DB _083DUBIOUS_COLUMNS
DB _084DUCE_COLUMNS
DB _085TREY_COLUMNS
DB _086SEAL_COLUMNS
DB _087SEALION_COLUMNS
DB _088GREASER_COLUMNS
DB _089YUCK_COLUMNS
DB _090CUP_COLUMNS
DB _091DISH_COLUMNS
DB _092GAS_COLUMNS
DB _093MORTY_COLUMNS
DB _094MACOBB_COLUMNS
DB _095ONYX_COLUMNS
DB _096ZEE_COLUMNS
DB _097MEDIUM_COLUMNS
DB _098DICK_COLUMNS
DB _099REX_COLUMNS
DB _100SPEEDBALL_COLUMNS
DB _101TUCKER_COLUMNS
DB _102ED_COLUMNS
DB _103EDD_COLUMNS
DB _104BONES_COLUMNS
DB _105MARROW_COLUMNS
DB _106BRUCE_COLUMNS
DB _107JACKIE_COLUMNS
DB _108TONGUE_COLUMNS

PlayerTileMaps3::
DW _082TonTileMap
DW _083DubiousTileMap
DW _084DuceTileMap
DW _085TreyTileMap
DW _086SealTileMap
DW _087SealionTileMap
DW _088GreaserTileMap
DW _089YuckTileMap
DW _090CupTileMap
DW _091DishTileMap
DW _092GasTileMap
DW _093MortyTileMap
DW _094MacobbTileMap
DW _095OnyxTileMap
DW _096ZeeTileMap
DW _097MediumTileMap
DW _098DickTileMap
DW _099RexTileMap
DW _100SpeedballTileMap
DW _101TuckerTileMap
DW _102EdTileMap
DW _103EddTileMap
DW _104BonesTileMap
DW _105MarrowTileMap
DW _106BruceTileMap
DW _107JackieTileMap
DW _108TongueTileMap

SECTION "Player Images 4", ROMX, BANK[PLAYER_IMG_BANK+4]

DW PlayerTiles4
DW PlayerTileCounts4
DW PlayerColumns4
DW PlayerTileMaps4
INCLUDE "img/players/109Ash.asm"
INCLUDE "img/players/110Smokey.asm"
INCLUDE "img/players/111Rhino.asm"
INCLUDE "img/players/112Don.asm"
INCLUDE "img/players/113Chance.asm"
INCLUDE "img/players/114Frizz.asm"
INCLUDE "img/players/115Gengis.asm"
INCLUDE "img/players/116Brain.asm"
INCLUDE "img/players/117Einstein.asm"
INCLUDE "img/players/118Goldie.asm"
INCLUDE "img/players/119Seeker.asm"
INCLUDE "img/players/120Starchild.asm"
INCLUDE "img/players/121Starman.asm"
INCLUDE "img/players/122Mime.asm"
INCLUDE "img/players/123Scissors.asm"
INCLUDE "img/players/124Jinx.asm"
INCLUDE "img/players/125Buzz.asm"
INCLUDE "img/players/126Magma.asm"
INCLUDE "img/players/127Pinch.asm"
INCLUDE "img/players/128Bull.asm"
INCLUDE "img/players/129Fish.asm"
INCLUDE "img/players/130Gar.asm"
INCLUDE "img/players/131Bus.asm"
INCLUDE "img/players/132YeahYeah.asm"
INCLUDE "img/players/133Evie.asm"
INCLUDE "img/players/134Vapor.asm"
INCLUDE "img/players/135Jolt.asm"

PlayerTiles4::
DW _109AshTiles
DW _110SmokeyTiles
DW _111RhinoTiles
DW _112DonTiles
DW _113ChanceTiles
DW _114FrizzTiles
DW _115GengisTiles
DW _116BrainTiles
DW _117EinsteinTiles
DW _118GoldieTiles
DW _119SeekerTiles
DW _120StarchildTiles
DW _121StarmanTiles
DW _122MimeTiles
DW _123ScissorsTiles
DW _124JinxTiles
DW _125BuzzTiles
DW _126MagmaTiles
DW _127PinchTiles
DW _128BullTiles
DW _129FishTiles
DW _130GarTiles
DW _131BusTiles
DW _132YeahYeahTiles
DW _133EvieTiles
DW _134VaporTiles
DW _135JoltTiles

PlayerTileCounts4::
DB _109ASH_TILE_COUNT
DB _110SMOKEY_TILE_COUNT
DB _111RHINO_TILE_COUNT
DB _112DON_TILE_COUNT
DB _113CHANCE_TILE_COUNT
DB _114FRIZZ_TILE_COUNT
DB _115GENGIS_TILE_COUNT
DB _116BRAIN_TILE_COUNT
DB _117EINSTEIN_TILE_COUNT
DB _118GOLDIE_TILE_COUNT
DB _119SEEKER_TILE_COUNT
DB _120STARCHILD_TILE_COUNT
DB _121STARMAN_TILE_COUNT
DB _122MIME_TILE_COUNT
DB _123SCISSORS_TILE_COUNT
DB _124JINX_TILE_COUNT
DB _125BUZZ_TILE_COUNT
DB _126MAGMA_TILE_COUNT
DB _127PINCH_TILE_COUNT
DB _128BULL_TILE_COUNT
DB _129FISH_TILE_COUNT
DB _130GAR_TILE_COUNT
DB _131BUS_TILE_COUNT
DB _132YEAHYEAH_TILE_COUNT
DB _133EVIE_TILE_COUNT
DB _134VAPOR_TILE_COUNT
DB _135JOLT_TILE_COUNT

PlayerColumns4::
DB _109ASH_COLUMNS
DB _110SMOKEY_COLUMNS
DB _111RHINO_COLUMNS
DB _112DON_COLUMNS
DB _113CHANCE_COLUMNS
DB _114FRIZZ_COLUMNS
DB _115GENGIS_COLUMNS
DB _116BRAIN_COLUMNS
DB _117EINSTEIN_COLUMNS
DB _118GOLDIE_COLUMNS
DB _119SEEKER_COLUMNS
DB _120STARCHILD_COLUMNS
DB _121STARMAN_COLUMNS
DB _122MIME_COLUMNS
DB _123SCISSORS_COLUMNS
DB _124JINX_COLUMNS
DB _125BUZZ_COLUMNS
DB _126MAGMA_COLUMNS
DB _127PINCH_COLUMNS
DB _128BULL_COLUMNS
DB _129FISH_COLUMNS
DB _130GAR_COLUMNS
DB _131BUS_COLUMNS
DB _132YEAHYEAH_COLUMNS
DB _133EVIE_COLUMNS
DB _134VAPOR_COLUMNS
DB _135JOLT_COLUMNS

PlayerTileMaps4::
DW _109AshTileMap
DW _110SmokeyTileMap
DW _111RhinoTileMap
DW _112DonTileMap
DW _113ChanceTileMap
DW _114FrizzTileMap
DW _115GengisTileMap
DW _116BrainTileMap
DW _117EinsteinTileMap
DW _118GoldieTileMap
DW _119SeekerTileMap
DW _120StarchildTileMap
DW _121StarmanTileMap
DW _122MimeTileMap
DW _123ScissorsTileMap
DW _124JinxTileMap
DW _125BuzzTileMap
DW _126MagmaTileMap
DW _127PinchTileMap
DW _128BullTileMap
DW _129FishTileMap
DW _130GarTileMap
DW _131BusTileMap
DW _132YeahYeahTileMap
DW _133EvieTileMap
DW _134VaporTileMap
DW _135JoltTileMap

SECTION "Player Images 5", ROMX, BANK[PLAYER_IMG_BANK+5]

DW PlayerTiles5
DW PlayerTileCounts5
DW PlayerColumns5
DW PlayerTileMaps5
INCLUDE "img/players/136Falre.asm"
INCLUDE "img/players/137Polygon.asm"
INCLUDE "img/players/138Spiral.asm"
INCLUDE "img/players/139Spire.asm"
INCLUDE "img/players/140Horshoe.asm"
INCLUDE "img/players/141Topper.asm"
INCLUDE "img/players/142Arrowhead.asm"
INCLUDE "img/players/143Bear.asm"
INCLUDE "img/players/144Art.asm"
INCLUDE "img/players/145Zaph.asm"
INCLUDE "img/players/146Elton.asm"
INCLUDE "img/players/147Drats.asm"
INCLUDE "img/players/148Aire.asm"
INCLUDE "img/players/149Dragon.asm"
INCLUDE "img/players/150Mike.asm"
INCLUDE "img/players/151Mick.asm"

PlayerTiles5::
DW _136FalreTiles
DW _137PolygonTiles
DW _138SpiralTiles
DW _139SpireTiles
DW _140HorshoeTiles
DW _141TopperTiles
DW _142ArrowheadTiles
DW _143BearTiles
DW _144ArtTiles
DW _145ZaphTiles
DW _146EltonTiles
DW _147DratsTiles
DW _148AireTiles
DW _149DragonTiles
DW _150MikeTiles
DW _151MickTiles

PlayerTileCounts5::
DB _136FALRE_TILE_COUNT
DB _137POLYGON_TILE_COUNT
DB _138SPIRAL_TILE_COUNT
DB _139SPIRE_TILE_COUNT
DB _140HORSHOE_TILE_COUNT
DB _141TOPPER_TILE_COUNT
DB _142ARROWHEAD_TILE_COUNT
DB _143BEAR_TILE_COUNT
DB _144ART_TILE_COUNT
DB _145ZAPH_TILE_COUNT
DB _146ELTON_TILE_COUNT
DB _147DRATS_TILE_COUNT
DB _148AIRE_TILE_COUNT
DB _149DRAGON_TILE_COUNT
DB _150MIKE_TILE_COUNT
DB _151MICK_TILE_COUNT

PlayerColumns5::
DB _136FALRE_COLUMNS
DB _137POLYGON_COLUMNS
DB _138SPIRAL_COLUMNS
DB _139SPIRE_COLUMNS
DB _140HORSHOE_COLUMNS
DB _141TOPPER_COLUMNS
DB _142ARROWHEAD_COLUMNS
DB _143BEAR_COLUMNS
DB _144ART_COLUMNS
DB _145ZAPH_COLUMNS
DB _146ELTON_COLUMNS
DB _147DRATS_COLUMNS
DB _148AIRE_COLUMNS
DB _149DRAGON_COLUMNS
DB _150MIKE_COLUMNS
DB _151MICK_COLUMNS

PlayerTileMaps5::
DW _136FalreTileMap
DW _137PolygonTileMap
DW _138SpiralTileMap
DW _139SpireTileMap
DW _140HorshoeTileMap
DW _141TopperTileMap
DW _142ArrowheadTileMap
DW _143BearTileMap
DW _144ArtTileMap
DW _145ZaphTileMap
DW _146EltonTileMap
DW _147DratsTileMap
DW _148AireTileMap
DW _149DragonTileMap
DW _150MikeTileMap
DW _151MickTileMap

SECTION "Player Animations 0", ROMX, BANK[PLAYER_IMG_BANK+IMG_BANK_COUNT+0]


INCLUDE "img/players/001Bubbi/001Bubbi_righty_pitcher_opponent.asm"
INCLUDE "img/players/001Bubbi/001Bubbi_lefty_pitcher_opponent.asm"
INCLUDE "img/players/001Bubbi/001Bubbi_righty_pitcher_user_2x.asm"
INCLUDE "img/players/001Bubbi/001Bubbi_righty_batter_user.asm"
INCLUDE "img/players/001Bubbi/001Bubbi_lefty_batter_opponent.asm"
INCLUDE "img/players/001Bubbi/001Bubbi_lefty_batter_user.asm"
INCLUDE "img/players/002Ivy/002Ivy_lefty_batter_opponent.asm"
INCLUDE "img/players/002Ivy/002Ivy_righty_pitcher_opponent.asm"
INCLUDE "img/players/003Venus/003Venus_righty_pitcher_opponent.asm"
INCLUDE "img/players/003Venus/003Venus_lefty_batter_user.asm"
INCLUDE "img/players/003Venus/003Venus_righty_batter_user.asm"
INCLUDE "img/players/004Ginger/004Ginger_lefty_pitcher_user_2x.asm"
INCLUDE "img/players/004Ginger/004Ginger_lefty_pitcher_opponent.asm"
SECTION "Player Animations 1", ROMX, BANK[PLAYER_IMG_BANK+IMG_BANK_COUNT+1]


INCLUDE "img/players/005Freckles/005Freckles_lefty_pitcher_user_2x.asm"
INCLUDE "img/players/005Freckles/005Freckles_lefty_pitcher_opponent.asm"
INCLUDE "img/players/006BigRed/006BigRed_lefty_pitcher_opponent.asm"
INCLUDE "img/players/006BigRed/006BigRed_lefty_pitcher_user.asm"
INCLUDE "img/players/007Squirt/007Squirt_righty_pitcher_opponent.asm"
INCLUDE "img/players/009Yogi/009Yogi_righty_batter_user.asm"
INCLUDE "img/players/026Rai/026Rai_lefty_pitcher_opponent.asm"
INCLUDE "img/players/143Bear/143Bear_lefty_pitcher_user_2x.asm"
INCLUDE "img/players/143Bear/143Bear_righty_batter_user.asm"
INCLUDE "img/players/143Bear/143Bear_righty_batter_opponent.asm"
INCLUDE "img/players/145Zaph/145Zaph_righty_batter_user.asm"
