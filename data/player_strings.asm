SECTION "Player Strings", ROMX, BANK[PLAYER_STRINGS_BANK]

PlayerNames::
DB "BUBBI", 0
DB "IVY", 0
DB "VENUS", 0
DB "GINGER", 0
DB "FRECKLES", 0
DB "BIG RED", 0
DB "SQUIRT", 0
DB "PUDGE", 0
DB "YOGI", 0
DB "SWEET PEA", 0
DB "META", 0
DB "BUTTERFLY", 0
DB "WEEDS", 0
DB "KOO-KOO", 0
DB "BEEDIE", 0
DB "PIDGE", 0
DB "GIO", 0
DB "OT", 0
DB "RAT TAIL", 0
DB "CAIT", 0
DB "STOOP", 0
DB "CROW", 0
DB "SNAKE", 0
DB "COBRA", 0
DB "CHU", 0
DB "RAI", 0
DB "SHREW", 0
DB "SANDY", 0
DB "CHICA", 0
DB "MUCHACHA", 0
DB "REINA", 0
DB "CHICO", 0
DB "MUCHACHO", 0
DB "RAY", 0
DB "FAIRY", 0
DB "FABLE", 0
DB "VULFPEK", 0
DB "FOXY", 0
DB "PUFF", 0
DB "TUFF", 0
DB "BATS", 0
DB "GOLD BATS", 0
DB "ODDIE", 0
DB "GLOOMY", 0
DB "VILE", 0
DB "PARIS", 0
DB "PARIAH", 0
DB "NATTY", 0
DB "MOTHER", 0
DB "DIGGS", 0
DB "DOUG", 0
DB "MEOW", 0
DB "PURR", 0
DB "DUCK", 0
DB "GOLD DUCK", 0
DB "MAN", 0
DB "PRIMO", 0
DB "LITHIUM", 0
DB "ARCANE", 0
DB "POLLY", 0
DB "SWIRL", 0
DB "WRATH", 0
DB "BRUH", 0
DB "DAB", 0
DB "KAZAA", 0
DB "CHIP", 0
DB "CHOKE", 0
DB "CHAMP", 0
DB "SPROUT", 0
DB "WEEPY", 0
DB "VIC", 0
DB "FONZ", 0
DB "CRUEL", 0
DB "DUDE", 0
DB "GRAVEL", 0
DB "GOLEM", 0
DB "PINTO", 0
DB "MUSTANG", 0
DB "SLOWPOKE", 0
DB "LAGGARD", 0
DB "MAGNET", 0
DB "TON", 0
DB "DUBIOUS", 0
DB "DUCE", 0
DB "TREY", 0
DB "SEAL", 0
DB "SEALION", 0
DB "GREASER", 0
DB "YUCK", 0
DB "CUP", 0
DB "DISH", 0
DB "GAS", 0
DB "MORTY", 0
DB "MACOBB", 0
DB "ONYX", 0
DB "ZEE", 0
DB "MEDIUM", 0
DB "DICK", 0
DB "REX", 0
DB "SPEEDBALL", 0
DB "TUCKER", 0
DB "ED", 0
DB "EDD", 0
DB "BONES", 0
DB "MARROW", 0
DB "BRUCE", 0
DB "JACKIE", 0
DB "TONGUE", 0
DB "ASH", 0
DB "SMOKEY", 0
DB "RHINO", 0
DB "DON", 0
DB "CHANCE", 0
DB "FRIZZ", 0
DB "GENGIS", 0
DB "BRAIN", 0
DB "EINSTEIN", 0
DB "GOLDIE", 0
DB "SEEKER", 0
DB "STARCHILD", 0
DB "STARMAN", 0
DB "MIME", 0
DB "SCISSORS", 0
DB "JINX", 0
DB "BUZZ", 0
DB "MAGMA", 0
DB "PINCH", 0
DB "BULL", 0
DB "FISH", 0
DB "GAR", 0
DB "BUS", 0
DB "YEAH-YEAH", 0
DB "EVIE", 0
DB "VAPOR", 0
DB "JOLT", 0
DB "FALRE", 0
DB "POLYGON", 0
DB "SPIRAL", 0
DB "SPIRE", 0
DB "HORSHOE", 0
DB "TOPPER", 0
DB "ARROWHEAD", 0
DB "BEAR", 0
DB "ART", 0
DB "ZAPH", 0
DB "ELTON", 0
DB "DRATS", 0
DB "AIRE", 0
DB "DRAGON", 0
DB "MIKE", 0
DB "MICK", 0


PlayerDescriptions::
DB "A strange kid.\nWon't wear hats.\nPlants and throws\nhard. PLAYER is a\ngrowing talent.", 0
DB "When their hair is\ntied back, they\nappear to lose the\nability to stand\nup right", 0
DB "Seems to bloom\nwhen it's sunny\noutside. Stays on\nthe move in the\ninfield.", 0
DB "Easily sunburns.\nWhen it rains,\nsteam is said to\nraise from their\nhead.", 0
DB "When they swing a\nhot bat, they\nelevate the ball\nto unbearably high\nlevels.", 0
DB "Throws fire that\nis hot enough to\nbreak ankles.\nKnown for their\nhigh heat.", 0
DB "In extra innings,\nthey swell up and\nharden into a\nwall. Powerfully\nsprays singles.", 0
DB "Often hides\nsignals and picks\noff unwary prey.\nFor blocking fast,\nthey move their\nheals to maintain\nbalance.", 0
DB "A brutal PLAYER.\nGood under\npressure. A cannon\nbehind the plate.\nThe core of a good\ninfield.", 0
DB "Their short feet\nare tipped with\ncleats. Steals\nsecond and third\neffortlessly.", 0
DB "This PLAYER is\nvulnerable to\npitches. Their\nopen stance\nexposes their weak\nand tender body.", 0
DB "At the plate, they\nflip their\nincredible hair,\nsending highly\ntoxic dust into\nthe air.", 0
DB "Often found in\nparks, chewing\nleaves. They have\na sharp venomous\nslider.", 0
DB "Almost incapable\nof swinging, this\nPLAYER can only\ncrowd the plate,\ndig in, and brace\nfor impact.", 0
DB "Flies at high\nspeed and attacks\nthe ball with a\nlarge venomous\nswing. Massive\nforearms and\nhands.", 0
DB "A common sight in\nparks and batting\ncages. They run\nlow to the ground\nand kick up\nblinding sand.", 0
DB "Very protective of\ntheir corner of\nthe field, this\nPLAYER will\nfiercely charge\nany ball.", 0
DB "When pitching,\nthey spin off the\nmound at high\nspeed to pick off\nunwary runners\nsuch as FISH.", 0
DB "Bite their lip\nwhen they bat.\nSmall and very\nquick, it is a\ncommon sight in\nmany places", 0
DB "A gym rat with a\npower mullet. They\napparently slow\ndown after a hair\ncut.", 0
DB "Eats grounders in\ngrassy areas. They\nhave to move their\nshort legs at high\nspeed to steal\nbases.", 0
DB "With their huge\nand magnificent\nswings, they hit\nfly balls that\nseem to disappear.", 0
DB "Runs silently and\nstealthily. Runs\non pitchers, such\nas PIDGE and\nSTOOP, any time\nthey're on base.", 0
DB "It is rumored that\nthe scarry looking\ntattoos on their\nfaces show their\nneighborhood\naffiliation", 0
DB "When several of\nthese PLAYERS play\ninflied, their\nelectricity turns\nlightning fast\ndouble plays.", 0
DB "Their long stride\ncovers a lot of\nground to protect\nfrom high fly\nballs.", 0
DB "Digs deep in the\nbox. It only\nswings at the\nright pitch.", 0
DB "Sits back on\nspinning balls.\nAttacks when\nthreatened inside.\nThey can roll a\nbunt up the line.", 0
DB "Although small,\ntheir venomous\nbunts render this\nPLAYER dangerous.\nThis girl has\nspeed.", 0
DB "Quick around the\nhorn. A physical\nPLAYER. Good\nspeed. Controlled\nbunt.", 0
DB "Hard hitter.\nStrong arm. She\nuses her hefty\nbulk to muscle out\nhome runs.", 0
DB "Stiffens up when\nthey strike out\nbatters. The\nlarger its hands,\nthe more powerful\nits venomous\nslurve.", 0
DB "An aggressive\nPLAYER that is\nquick to attack\nstrikes. On the\nmound they sling\nvenom", 0
DB "It uses its\npowerful arms on\nthe mound to heave\nfastballs, spin\ncurves, and break\nthe break the\nbatter's ankles.", 0
DB "Their magical and\ncute appeal has\nmany fans. They're\nrare and found\nonly in certain\nareas", 0
DB "A timid fairy\nPLAYER that is\nrarely seen. They\nwill run and hide\nthe moment it\nsenses people", 0
DB "Rookie year, they\nhave just one\nponytail. Their\nhair splits at the\ntip as they grow\nolder", 0
DB "Very smart and\nvery vengeful.\nSwinging one of\ntheir many bats\ncould result in a\n1000-year curse", 0
DB "When their huge\neyes light up,\nthey sings a\nmysteriously\nsoothing melody\nthat lulls runners\nto sleep", 0
DB "Their body is soft\nand rubbery. When\nbatting, they puff\nup their chest in\nan attempt to look\nbigger.", 0
DB "A blind PLAYER.\nUses echo location\nto find the ball.", 0
DB "Once it strikes\nout a batter, it\nwill not stop\ndraining energy\nfrom the other\nteam, even late in\nthe game.", 0
DB "During the day,\nthey practice\nfielding ground\nballs. At night,\nthey run bases.", 0
DB "The wet stuff on\ntheir mouth isn't\ndrool. It is\nlipgloss that is\nused to attract\nopposing players.", 0
DB "The larger their\nhair, the more\ntoxic hair product\nit contains. Their\nbig head is heavy\nand hard to hold\nup", 0
DB "Drinks ginger tea.\nThe headphones\nover their hats\nkeep them zoned\ninto the game.", 0
DB "A host of mental\nproblems brought\non by parasites\nand psychadellic\nmushrooms. Prefers\nto play in a\ndress.", 0
DB "Lives in the\nshadow of their\nMOTHER. Attracted\nby the bright\nlights of game\nnight.", 0
DB "The dusty\nsubstances\ncovering their\nuniform indicates\nwhich types of\npoison they throw.", 0
DB "Could field a\nground ball a yard\nunder the dirt.", 0
DB "The hard chopping\nmotion in their\nswing can trigger\nearthquakes 60\nmiles from the\nball's impact\npoint", 0
DB "Adores spherical\nobjects. Wanders\nthe streets on a\nnightly basis to\nlook for foul\nballs.", 0
DB "Although their\ngame has many\nadmirers, they're\ntough to keep on a\nteam because of\ntheir fickle\nmeanness.", 0
DB "While lulling the\nbatter with a\nvacant look, this\nwily PLAYER will\nuse psychokinetic\npowers", 0
DB "Often seen swim-\nming elegantly by\nlake shores. It is\noften mistaken for\nthe Japanese\nmonster, Kappa", 0
DB "Extremely quick to\nanger. It could be\ndocile one moment\nthen thrashing\naway the next\ninstant", 0
DB "Always furious and\ntenacious to boot.\nIt will not\nabandon chasing\nits quarry until\nit is caught", 0
DB "Very protective of\ntheir strikezone.\nThey will pitch up\nand in to repel\nbatters from their\nplate.", 0
DB "A PLAYER admired\nfor their\nbeautiful swing.\nRuns agilely as if\non wings", 0
DB "Their newly grown\nlegs prevent them\nfrom running. They\nappear to prefer\nswinging for the\nfences than trying\nfor a base hit.", 0
DB "Capable of batting\nleft or hight\nhanded. When out\nof the box, it\nsweats to keep its\nbody cool", 0
DB "An adept swimmer\nat both the front\ncrawl and breast\nstroke. Easily\novertakes the best\nhuman swimmers", 0
DB "Using their\nability to read\nminds, they can\npredict what the\npitcher will\nthrow.", 0
DB "It emits special\nalpha waves from\nits body that\ninduce headaches\njust by being\nclose by", 0
DB "Their brains can\noutperform any\nadvanced analytics\nsoftware. Their\n#BOL knowledge is\nthe highest in the\nLEAGUE.", 0
DB "Loves to build its\nmuscles. It trains\nin all styles of\nmartial arts to\nbecome even\nstronger", 0
DB "Its muscular body\nis so powerful, it\nmust wear a power\nsave belt to be\nable to regulate\nits motions", 0
DB "Using its heavy\nmuscles, it throws\npowerful punches\nthat can send the\nvictim clear over\nthe horizon", 0
DB "A carnivorous\nPOKéMON that traps\nand eats bugs. It\nuses its root feet\nto soak up needed\nmoisture", 0
DB "It spits out\nPOISONPOWDER to\nimmobilize the\nenemy and then\nfinishes it with a\nspray of ACID", 0
DB "Said to live in\nhuge colonies deep\nin jungles,\nalthough no one\nhas ever returned\nfrom there", 0
DB "Drifts in shallow\nseas. Anglers who\nhook them by\naccident are often\npunished by its\nstinging acid", 0
DB "The tentacles are\nnormally kept\nshort. On hunts,\nthey are extended\nto ensnare and\nimmobilize prey", 0
DB "Found in fields\nand mountains.\nMistaking them for\nboulders, people\noften step or trip\non them", 0
DB "Rolls down slopes\nto move. It rolls\nover any obstacle\nwithout slowing or\nchanging its\ndirection", 0
DB "Its boulder-like\nbody is extremely\nhard. It can\neasily withstand\ndynamite blasts\nwithout damage", 0
DB "Its hooves are 10\ntimes harder than\ndiamonds. It can\ntrample anything\ncompletely flat in\nlittle time", 0
DB "Very competitive,\nthis POKéMON will\nchase anything\nthat moves fast in\nthe hopes of\nracing it", 0
DB "Incredibly slow\nand dopey. It\ntakes 5 seconds\nfor it to feel\npain when under\nattack", 0
DB "The SHELLDER that\nis latched onto\nSLOWPOKE's tail is\nsaid to feed on\nthe host's left\nover scraps", 0
DB "Uses anti-gravity\nto stay suspended.\nAppears without\nwarning and uses\nTHUNDER WAVE and\nsimilar moves", 0
DB "Formed by several\nMAGNETs practicing\ntogether. They\nfrequently appear\nin the outfield\nspottings flares", 0
DB "The sprig of green\nonions it holds is\nits weapon. It is\nused much like a\nmetal sword", 0
DB "A bird that makes\nup for its poor\nflying with its\nfast foot speed.\nLeaves giant\nfootprints", 0
DB "Uses its three\nbrains to execute\ncomplex plans.\nWhile two heads\nsleep, one head\nstays awake", 0
DB "The protruding\nhorn on its head\nis very hard. It\nis used for\nbashing through\nthick ice", 0
DB "Stores thermal\nenergy in its\nbody. Swims at a\nsteady 8 knots\neven in intensely\ncold waters", 0
DB "Lives in slums and\nabandoned houses.\nSurvives by\nselling the toxic\nsludge dumped by\nfactories on the\nblack market.", 0
DB "Thickly covered\nwith a filthy,\nvile sludge. It is\nso toxic, even its\nfootprints contain\npoison", 0
DB "Its hard shell\nrepels any kind of\nattack. It is\nvulnerable only\nwhen its shell is\nopen", 0
DB "When attacked, it\nlaunches its horns\nin quick volleys.\nIts innards have\nnever been seen", 0
DB "Almost invisible,\nthis gaseous\nPLAYER cloaks the\ntarget and puts it\nto sleep without\nnotice", 0
DB "Because of its\nability to slip\nthrough block\nwalls, it is said\nto be from an-\nother dimension", 0
DB "Under a full moon,\nthis PLAYER likes\nto mimic the\nshadows of people\nand laugh at their\nfright", 0
DB "As it grows, the\nstone portions of\nits body harden to\nbecome similar to\na diamond, but\ncolored black", 0
DB "Puts enemies to\nsleep then eats\ntheir dreams.\nOccasionally gets\nsick from eating\nbad dreams", 0
DB "When it locks eyes\nwith an enemy, it\nwill use a mix of\nPSI moves such as\nHYPNOSIS and\nCONFUSION", 0
DB "Its pincers are\nnot only powerful\nweapons, they are\nused for balance\nwhen walking\nsideways", 0
DB "The large pitcher\nthrows 100 mph and\nhas crushing\npower. However,\nits huge size\nmakes it unable to\nfield", 0
DB "Usually found in\npower plants.\nEasily mistaken\nfor a # BALL, they\nhave zapped many\npeople", 0
DB "It stores electric\nenergy under very\nhigh pressure. It\noften explodes\nwith little or no\nprovocation", 0
DB "Often mistaken for\na group of people.\nA disturbed, one-\narmed man with\nlots of tattoos of\nhimself.", 0
DB "His other arm\nmiraculously grew\nback. Legend has\nit, the arm he\nlost grew a human,\ntoo.", 0
DB "Because it never\nremoves its giant\nhat, no one has\never seen this\nPLAYER's real face", 0
DB "Thier bat is the\nkey to their\nsuccess. They\noften throw their\nbat skillfully\nlike a boomerang\nmake contact with\nthe ball.", 0
DB "When in a hurry,\nits legs lengthen\nprogressively. It\nruns smoothly with\nextra long, loping\nstrides", 0
DB "While apparently\ndoing nothing, it\nfires punches in\nlightning fast\nvolleys that are\nimpossible to see", 0
DB "Its tongue can be\nextended like a\nchameleon's. It\nleaves a tingling\nsensation when it\nlicks enemies", 0
DB "Because they\ninhale several\nkinds of toxic\ngases, they're\nprone to explosive\ndiarrhea.", 0
DB "Where two kinds of\npoison gases meet,\n2 KOFFINGs can\nfuse into a\nWEEZING over many\nyears", 0
DB "Its massive bones\nare 1000 times\nharder than human\nbones. It can\neasily knock a\ntrailer flying", 0
DB "Protected by an\narmor-like hide,\nit is capable of\nliving in molten\nlava of 3,600\ndegrees", 0
DB "Genuinely cool.\nThe kind of PLAYER\neveryone wants on\ntheir TEAM. Can't\nhit or field the\nball.", 0
DB "Their whole body\nstinks of sea salt\nand weed. Their\nbeards shake as\nthey walk.", 0
DB "Wears a protective\npouch around her\nperpetually\npregnant belly.", 0
DB "Known to down\nflying bugs with\nprecision blasts\nof ink from the\nsurface of the\nwater", 0
DB "Capable of\nthrowing backwards\nby rapidly\nswinging their\nwing-like arms and\nstout legs", 0
DB "Their golden locks\nbillow like an\nelegant ballroom\ndress, giving them\nthe nickname of\nthe Queen of\nDiamonds", 0
DB "In the post-\nseason, they can\nbe seen swimming\npowerfully up\nrivers and creeks\nto train", 0
DB "An enigmatic\nPLAYER that can\neffortlessly\ngenerate a\ncheering crowd\neven when losing", 0
DB "A core figure on\nany roster with a\nglow like the\nseven colors of\nthe rainbow. Some\nfans value their\npersonality above\ntheir skill", 0
DB "Able to mirror\nevery movement on\nthe field. Knows\nwhere the ball is\ngoing before the\npitcher.", 0
DB "With ninja-like\nagility and speed,\nthey look like\nthey're playing\nmore than one\nposition.", 0
DB "The seductive\nwiggle in their\nhips as they pitch\ncan cause batters\nto forget to\nswing.", 0
DB "Normally found\nnear power plants,\nthe static\ndischarge of their\nswing can cause\nmajor blackouts in\ncities", 0
DB "Their heater\nalways burns with\nan orange glow\nthat enables it to\nhide perfectly\namong the lights\nof a scoreboard.", 0
DB "When they fail to\ncrush the ball,\nthey break their\nbat over their\nhead.", 0
DB "When hit by a\npitch, they charge\nthe mound and whip\nthe pitcher with\ntheir long arms.", 0
DB "Their parents\nplayed ball well,\nbut these PLAYERs\nclearly didn't get\nthe BéiSBOL gene.", 0
DB "Rarely seen in the\nwild. Huge,\nvicious, and\ncapable of\nknocking the cover\noff a ball.", 0
DB "A PLAYER that has\nbeen over- scouted\nalmost to\nextinction. It can\ncarry a team.", 0
DB "Capable of copying\nan opponents's\nstyle to instantly\ntransform\nthemselves into a\nduplicate of the\nopponent.", 0
DB "Their future is\nwide open. They\ncan grow up to be\nanything they want\nto be.", 0
DB "Lives at the\nbeach. They go on\nlong swims in the\ndeep ocean and are\noften mistaken for\nmermaids", 0
DB "They accumulate\nnegative ions in\ntheir hair. It\ngives a little\nextra zip to their\nthrows.", 0
DB "In the moments\nbefore each pitch,\nthey get an\nadrenaline rush.", 0
DB "A PLAYER who\nspends a lot of\ntime writing code.\nThey often run the\nteam website and\nkeep stats.", 0
DB "Although long\nretired, in rare\ncases, these early\npioneers of\nbatting helmets\nwill play ball.", 0
DB "A deadball era\nPLAYER that\nretired when the\nheavy equipment\nmade it impossible\nto catch a wild\npitch", 0
DB "A PLAYER that was\nresurrected from a\ngrave in what was\nonce Elysian\nFields.", 0
DB "Their sleek shape\nis perfect for\npitching. They\nstrike out batters\nwith a sharp curve\nand a fluid\nwindup.", 0
DB "A relic of early\nBéiSBOL when\npitchers would\nthrow at your\ntemple if you\ncrowded the plate.", 0
DB "Lazy bums just eat\nand sleep. As\ntheir rotund bulk\nbuilds, they\nbecome steadily\nmore slothful", 0
DB "A legendary\nPLAYER. They're\nsaid to appear to\ndoomed batters who\nare in a  slump", 0
DB "A legendary\nPLAYER. They're\nsaid to appear\nfrom the mist\nwhile dropping\nenormous lightning\nbolts to center", 0
DB "Known as legendary\nfireballers. Every\npitch creates a\ndazzling flash of\nflames", 0
DB "Long considered a\nmythical PLAYER\nuntil recently\nwhen a small\nbarnstorming team\nwas found playing\nin a remote field.", 0
DB "A mystical PLAYER\nthat exudes a\ngentle aura. Good\nweather seems to\nfollow them\neverywhere.", 0
DB "An extremely\nrarely seen island\nPLAYER. Their\nBéiSBOL knowledge\nis said to belong\nin a higher\nleague.", 0
DB "MIKE was created\nby a scientist\nafter years of\nhorrific gene\nsplicing and DNA\nengineering\nexperiments", 0
DB "Some claim they're\nstill barnstorming\nthe countryside.\nOnly a few left\ncan say they saw\nthem in LEAGUE\nplay.", 0

