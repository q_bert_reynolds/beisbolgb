SECTION "Move Strings", ROMX, BANK[PLAYER_STRINGS_BANK]

MoveNames::
DB "Pinpoint", 0
DB "Thread", 0
DB "Silky Swing", 0
DB "Dragon Bunt", 0
DB "Dragonball", 0
DB "Elec. Slider", 0
DB "LightninBolt", 0
DB "Shocker", 0
DB "Thunderball", 0
DB "ThunderStick", 0
DB "Beanball", 0
DB "Brushback", 0
DB "Comebacker", 0
DB "Lean In", 0
DB "Taunt", 0
DB "Fireball", 0
DB "Heater", 0
DB "High Heat", 0
DB "Scorch", 0
DB "Bender", 0
DB "Fly", 0
DB "Riser", 0
DB "Mock", 0
DB "Sky Smash", 0
DB "BafflingBunt", 0
DB "Dead Pull", 0
DB "Lick", 0
DB "Possess", 0
DB "Kudzuball", 0
DB "Grassburner", 0
DB "Hardwood", 0
DB "Sleeper", 0
DB "Plant n Turn", 0
DB "Bouncer", 0
DB "Chop Down", 0
DB "Crack", 0
DB "Dig In", 0
DB "Kickup Sand", 0
DB "Spelunker", 0
DB "ColdShoulder", 0
DB "Frozen Rope", 0
DB "Shiver", 0
DB "Bunt", 0
DB "Push Bunt", 0
DB "Drag Bunt", 0
DB "Barrage", 0
DB "Bash", 0
DB "Bomb", 0
DB "Changeup", 0
DB "Chant", 0
DB "CircleChange", 0
DB "Curveball", 0
DB "Cut", 0
DB "Cutter", 0
DB "Eephus Pitch", 0
DB "Explode", 0
DB "Fastball", 0
DB "Flash", 0
DB "Focus", 0
DB "Forkball", 0
DB "Fosh", 0
DB "Four-Seam", 0
DB "Glare", 0
DB "Gyroball", 0
DB "Harden", 0
DB "Knuckleball", 0
DB "KnuckleCurve", 0
DB "Moneyball", 0
DB "Palmball", 0
DB "Quick Swing", 0
DB "Rage", 0
DB "Scowl", 0
DB "Screwball", 0
DB "Seppukuball", 0
DB "Shuuto", 0
DB "Sinker", 0
DB "Slash", 0
DB "Slider", 0
DB "Slurve", 0
DB "Smash", 0
DB "Soft Toss", 0
DB "Splitter", 0
DB "Stomp", 0
DB "Strength", 0
DB "Struggle", 0
DB "Study", 0
DB "Submarine", 0
DB "Swat", 0
DB "Swing", 0
DB "Three-Seam", 0
DB "Toss", 0
DB "Two-Seam", 0
DB "Vulcan", 0
DB "Wiff", 0
DB "Filth", 0
DB "Nasty Curve", 0
DB "Gas", 0
DB "Puff", 0
DB "Blink", 0
DB "Brain Melter", 0
DB "Hypnosis", 0
DB "Kinesis", 0
DB "Meditate", 0
DB "Predict", 0
DB "Psych Out", 0
DB "Rest", 0
DB "Rock", 0
DB "Rock Hammer", 0
DB "Rock Slider", 0
DB "Bubbleball", 0
DB "Crabhammer", 0
DB "Hydro Cannon", 0
DB "Shell", 0
DB "Spitball", 0
DB "Surf", 0
DB "Withdraw", 0
