IF !DEF(_AVATARS_TILE_COUNT)
_AVATARS_TILE_COUNT EQU 23
_AvatarsTiles: INCBIN "img/avatars/avatars.tiles"
_BALL_ROWS EQU 2
_BALL_COLUMNS EQU 2
_BallTileMap: INCBIN "img/avatars/ball.tilemap"
_CalvinAvatarIdleDownTileMap: INCBIN "img/avatars/calvin_avatar_idle_down.tilemap"
_CalvinAvatarIdleDownPropMap: INCBIN "img/avatars/calvin_avatar_idle_down.propmap"
_CalvinAvatarIdleUpTileMap: INCBIN "img/avatars/calvin_avatar_idle_up.tilemap"
_CalvinAvatarIdleUpPropMap: INCBIN "img/avatars/calvin_avatar_idle_up.propmap"
_CalvinAvatarIdleRightTileMap: INCBIN "img/avatars/calvin_avatar_idle_right.tilemap"
_CalvinAvatarIdleRightPropMap: INCBIN "img/avatars/calvin_avatar_idle_right.propmap"
_CalvinAvatarWalkDownTileMap: INCBIN "img/avatars/calvin_avatar_walk_down.tilemap"
_CalvinAvatarWalkDownPropMap: INCBIN "img/avatars/calvin_avatar_walk_down.propmap"
_CalvinAvatarWalkUpTileMap: INCBIN "img/avatars/calvin_avatar_walk_up.tilemap"
_CalvinAvatarWalkUpPropMap: INCBIN "img/avatars/calvin_avatar_walk_up.propmap"
_CalvinAvatarWalkRightTileMap: INCBIN "img/avatars/calvin_avatar_walk_right.tilemap"
_CalvinAvatarWalkRightPropMap: INCBIN "img/avatars/calvin_avatar_walk_right.propmap"
ENDC
