IF !DEF(_VERSION_SPRITES_TILE_COUNT)
DEF _VERSION_SPRITES_TILE_COUNT EQU 118
_VersionSpritesTiles: INCBIN "img/home_version/version_sprites/version_sprites.tiles"
DEF _INTRO_WATCH0_ROWS EQU 6
DEF _INTRO_WATCH0_COLUMNS EQU 8
_IntroWatch0TileMap: INCBIN "img/home_version/version_sprites/intro_watch0.tilemap"
DEF _INTRO_WATCH1_ROWS EQU 6
DEF _INTRO_WATCH1_COLUMNS EQU 8
_IntroWatch1TileMap: INCBIN "img/home_version/version_sprites/intro_watch1.tilemap"
DEF _INTRO_SWING0_ROWS EQU 6
DEF _INTRO_SWING0_COLUMNS EQU 8
_IntroSwing0TileMap: INCBIN "img/home_version/version_sprites/intro_swing0.tilemap"
DEF _INTRO_SWING1_ROWS EQU 6
DEF _INTRO_SWING1_COLUMNS EQU 8
_IntroSwing1TileMap: INCBIN "img/home_version/version_sprites/intro_swing1.tilemap"
DEF _INTRO_STANCE0_ROWS EQU 6
DEF _INTRO_STANCE0_COLUMNS EQU 8
_IntroStance0TileMap: INCBIN "img/home_version/version_sprites/intro_stance0.tilemap"
DEF _INTRO_STANCE1_ROWS EQU 6
DEF _INTRO_STANCE1_COLUMNS EQU 8
_IntroStance1TileMap: INCBIN "img/home_version/version_sprites/intro_stance1.tilemap"
ENDC
