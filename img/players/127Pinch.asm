IF !DEF(_127PINCH_TILE_COUNT)
DEF _127PINCH_TILE_COUNT EQU 40
DEF _127PINCH_ROWS EQU 7
DEF _127PINCH_COLUMNS EQU 7
_127PinchTiles: INCBIN "img/players/127Pinch.tiles"
_127PinchTileMap: INCBIN "img/players/127Pinch.tilemap"
ENDC
