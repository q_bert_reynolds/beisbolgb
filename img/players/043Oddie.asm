IF !DEF(_043ODDIE_TILE_COUNT)
DEF _043ODDIE_TILE_COUNT EQU 23
DEF _043ODDIE_ROWS EQU 5
DEF _043ODDIE_COLUMNS EQU 5
_043OddieTiles: INCBIN "img/players/043Oddie.tiles"
_043OddieTileMap: INCBIN "img/players/043Oddie.tilemap"
ENDC
