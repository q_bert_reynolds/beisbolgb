IF !DEF(_049MOTHER_TILE_COUNT)
DEF _049MOTHER_TILE_COUNT EQU 39
DEF _049MOTHER_ROWS EQU 7
DEF _049MOTHER_COLUMNS EQU 7
_049MotherTiles: INCBIN "img/players/049Mother.tiles"
_049MotherTileMap: INCBIN "img/players/049Mother.tilemap"
ENDC
