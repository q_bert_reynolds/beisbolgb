IF !DEF(_010SWEETPEA_TILE_COUNT)
DEF _010SWEETPEA_TILE_COUNT EQU 18
DEF _010SWEETPEA_ROWS EQU 5
DEF _010SWEETPEA_COLUMNS EQU 5
_010SweetPeaTiles: INCBIN "img/players/010SweetPea.tiles"
_010SweetPeaTileMap: INCBIN "img/players/010SweetPea.tilemap"
ENDC
