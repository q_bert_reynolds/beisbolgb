IF !DEF(_020CAIT_TILE_COUNT)
DEF _020CAIT_TILE_COUNT EQU 33
DEF _020CAIT_ROWS EQU 6
DEF _020CAIT_COLUMNS EQU 6
_020CaitTiles: INCBIN "img/players/020Cait.tiles"
_020CaitTileMap: INCBIN "img/players/020Cait.tilemap"
ENDC
