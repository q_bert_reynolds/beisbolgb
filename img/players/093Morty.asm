IF !DEF(_093MORTY_TILE_COUNT)
DEF _093MORTY_TILE_COUNT EQU 36
DEF _093MORTY_ROWS EQU 6
DEF _093MORTY_COLUMNS EQU 6
_093MortyTiles: INCBIN "img/players/093Morty.tiles"
_093MortyTileMap: INCBIN "img/players/093Morty.tilemap"
ENDC
