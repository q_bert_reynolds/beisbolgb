IF !DEF(_078MUSTANG_TILE_COUNT)
DEF _078MUSTANG_TILE_COUNT EQU 36
DEF _078MUSTANG_ROWS EQU 7
DEF _078MUSTANG_COLUMNS EQU 7
_078MustangTiles: INCBIN "img/players/078Mustang.tiles"
_078MustangTileMap: INCBIN "img/players/078Mustang.tilemap"
ENDC
