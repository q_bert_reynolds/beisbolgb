IF !DEF(_149DRAGON_TILE_COUNT)
DEF _149DRAGON_TILE_COUNT EQU 39
DEF _149DRAGON_ROWS EQU 7
DEF _149DRAGON_COLUMNS EQU 7
_149DragonTiles: INCBIN "img/players/149Dragon.tiles"
_149DragonTileMap: INCBIN "img/players/149Dragon.tilemap"
ENDC
