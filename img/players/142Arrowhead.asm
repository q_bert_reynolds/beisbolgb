IF !DEF(_142ARROWHEAD_TILE_COUNT)
DEF _142ARROWHEAD_TILE_COUNT EQU 36
DEF _142ARROWHEAD_ROWS EQU 7
DEF _142ARROWHEAD_COLUMNS EQU 7
_142ArrowheadTiles: INCBIN "img/players/142Arrowhead.tiles"
_142ArrowheadTileMap: INCBIN "img/players/142Arrowhead.tilemap"
ENDC
