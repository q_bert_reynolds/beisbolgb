IF !DEF(_LEFTY_BATTER_USER_TILE_COUNT)
DEF _LEFTY_BATTER_USER_TILE_COUNT EQU 46
_LeftyBatterUserTiles: INCBIN "img/play/lefty_batter_user.tiles"
DEF _LEFTY_BATTER_USER0_ROWS EQU 7
DEF _LEFTY_BATTER_USER0_COLUMNS EQU 8
_LeftyBatterUser0TileMap: INCBIN "img/play/lefty_batter_user0.tilemap"
DEF _LEFTY_BATTER_USER1_ROWS EQU 7
DEF _LEFTY_BATTER_USER1_COLUMNS EQU 8
_LeftyBatterUser1TileMap: INCBIN "img/play/lefty_batter_user1.tilemap"
DEF _LEFTY_BATTER_USER2_ROWS EQU 7
DEF _LEFTY_BATTER_USER2_COLUMNS EQU 8
_LeftyBatterUser2TileMap: INCBIN "img/play/lefty_batter_user2.tilemap"
ENDC
