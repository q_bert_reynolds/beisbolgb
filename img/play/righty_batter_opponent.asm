IF !DEF(_RIGHTY_BATTER_OPPONENT_TILE_COUNT)
DEF _RIGHTY_BATTER_OPPONENT_TILE_COUNT EQU 62
_RightyBatterOpponentTiles: INCBIN "img/play/righty_batter_opponent.tiles"
DEF _RIGHTY_BATTER_OPPONENT0_ROWS EQU 7
DEF _RIGHTY_BATTER_OPPONENT0_COLUMNS EQU 8
_RightyBatterOpponent0TileMap: INCBIN "img/play/righty_batter_opponent0.tilemap"
DEF _RIGHTY_BATTER_OPPONENT1_ROWS EQU 7
DEF _RIGHTY_BATTER_OPPONENT1_COLUMNS EQU 8
_RightyBatterOpponent1TileMap: INCBIN "img/play/righty_batter_opponent1.tilemap"
DEF _RIGHTY_BATTER_OPPONENT2_ROWS EQU 7
DEF _RIGHTY_BATTER_OPPONENT2_COLUMNS EQU 8
_RightyBatterOpponent2TileMap: INCBIN "img/play/righty_batter_opponent2.tilemap"
ENDC
