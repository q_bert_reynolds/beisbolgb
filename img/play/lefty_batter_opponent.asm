IF !DEF(_LEFTY_BATTER_OPPONENT_TILE_COUNT)
DEF _LEFTY_BATTER_OPPONENT_TILE_COUNT EQU 57
_LeftyBatterOpponentTiles: INCBIN "img/play/lefty_batter_opponent.tiles"
DEF _LEFTY_BATTER_OPPONENT0_ROWS EQU 7
DEF _LEFTY_BATTER_OPPONENT0_COLUMNS EQU 8
_LeftyBatterOpponent0TileMap: INCBIN "img/play/lefty_batter_opponent0.tilemap"
DEF _LEFTY_BATTER_OPPONENT1_ROWS EQU 7
DEF _LEFTY_BATTER_OPPONENT1_COLUMNS EQU 8
_LeftyBatterOpponent1TileMap: INCBIN "img/play/lefty_batter_opponent1.tilemap"
DEF _LEFTY_BATTER_OPPONENT2_ROWS EQU 7
DEF _LEFTY_BATTER_OPPONENT2_COLUMNS EQU 8
_LeftyBatterOpponent2TileMap: INCBIN "img/play/lefty_batter_opponent2.tilemap"
ENDC
