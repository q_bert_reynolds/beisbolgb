IF !DEF(_RIGHTY_PITCHER_OPPONENT_TILE_COUNT)
DEF _RIGHTY_PITCHER_OPPONENT_TILE_COUNT EQU 52
_RightyPitcherOpponentTiles: INCBIN "img/play/righty_pitcher_opponent.tiles"
DEF _RIGHTY_PITCHER_OPPONENT0_ROWS EQU 7
DEF _RIGHTY_PITCHER_OPPONENT0_COLUMNS EQU 8
_RightyPitcherOpponent0TileMap: INCBIN "img/play/righty_pitcher_opponent0.tilemap"
DEF _RIGHTY_PITCHER_OPPONENT1_ROWS EQU 7
DEF _RIGHTY_PITCHER_OPPONENT1_COLUMNS EQU 8
_RightyPitcherOpponent1TileMap: INCBIN "img/play/righty_pitcher_opponent1.tilemap"
DEF _RIGHTY_PITCHER_OPPONENT2_ROWS EQU 7
DEF _RIGHTY_PITCHER_OPPONENT2_COLUMNS EQU 8
_RightyPitcherOpponent2TileMap: INCBIN "img/play/righty_pitcher_opponent2.tilemap"
DEF _RIGHTY_PITCHER_OPPONENT3_ROWS EQU 7
DEF _RIGHTY_PITCHER_OPPONENT3_COLUMNS EQU 8
_RightyPitcherOpponent3TileMap: INCBIN "img/play/righty_pitcher_opponent3.tilemap"
ENDC
