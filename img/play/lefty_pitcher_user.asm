IF !DEF(_LEFTY_PITCHER_USER_TILE_COUNT)
DEF _LEFTY_PITCHER_USER_TILE_COUNT EQU 49
_LeftyPitcherUserTiles: INCBIN "img/play/lefty_pitcher_user.tiles"
DEF _LEFTY_PITCHER_USER0_ROWS EQU 7
DEF _LEFTY_PITCHER_USER0_COLUMNS EQU 8
_LeftyPitcherUser0TileMap: INCBIN "img/play/lefty_pitcher_user0.tilemap"
DEF _LEFTY_PITCHER_USER1_ROWS EQU 7
DEF _LEFTY_PITCHER_USER1_COLUMNS EQU 8
_LeftyPitcherUser1TileMap: INCBIN "img/play/lefty_pitcher_user1.tilemap"
DEF _LEFTY_PITCHER_USER2_ROWS EQU 7
DEF _LEFTY_PITCHER_USER2_COLUMNS EQU 8
_LeftyPitcherUser2TileMap: INCBIN "img/play/lefty_pitcher_user2.tilemap"
DEF _LEFTY_PITCHER_USER3_ROWS EQU 7
DEF _LEFTY_PITCHER_USER3_COLUMNS EQU 8
_LeftyPitcherUser3TileMap: INCBIN "img/play/lefty_pitcher_user3.tilemap"
ENDC
