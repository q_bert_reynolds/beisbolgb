IF !DEF(_RIGHTY_PITCHER_USER_TILE_COUNT)
DEF _RIGHTY_PITCHER_USER_TILE_COUNT EQU 56
_RightyPitcherUserTiles: INCBIN "img/play/righty_pitcher_user.tiles"
DEF _RIGHTY_PITCHER_USER0_ROWS EQU 7
DEF _RIGHTY_PITCHER_USER0_COLUMNS EQU 8
_RightyPitcherUser0TileMap: INCBIN "img/play/righty_pitcher_user0.tilemap"
DEF _RIGHTY_PITCHER_USER1_ROWS EQU 7
DEF _RIGHTY_PITCHER_USER1_COLUMNS EQU 8
_RightyPitcherUser1TileMap: INCBIN "img/play/righty_pitcher_user1.tilemap"
DEF _RIGHTY_PITCHER_USER2_ROWS EQU 7
DEF _RIGHTY_PITCHER_USER2_COLUMNS EQU 8
_RightyPitcherUser2TileMap: INCBIN "img/play/righty_pitcher_user2.tilemap"
DEF _RIGHTY_PITCHER_USER3_ROWS EQU 7
DEF _RIGHTY_PITCHER_USER3_COLUMNS EQU 8
_RightyPitcherUser3TileMap: INCBIN "img/play/righty_pitcher_user3.tilemap"
ENDC
