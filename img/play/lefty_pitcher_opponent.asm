IF !DEF(_LEFTY_PITCHER_OPPONENT_TILE_COUNT)
DEF _LEFTY_PITCHER_OPPONENT_TILE_COUNT EQU 54
_LeftyPitcherOpponentTiles: INCBIN "img/play/lefty_pitcher_opponent.tiles"
DEF _LEFTY_PITCHER_OPPONENT0_ROWS EQU 7
DEF _LEFTY_PITCHER_OPPONENT0_COLUMNS EQU 8
_LeftyPitcherOpponent0TileMap: INCBIN "img/play/lefty_pitcher_opponent0.tilemap"
DEF _LEFTY_PITCHER_OPPONENT1_ROWS EQU 7
DEF _LEFTY_PITCHER_OPPONENT1_COLUMNS EQU 8
_LeftyPitcherOpponent1TileMap: INCBIN "img/play/lefty_pitcher_opponent1.tilemap"
DEF _LEFTY_PITCHER_OPPONENT2_ROWS EQU 7
DEF _LEFTY_PITCHER_OPPONENT2_COLUMNS EQU 8
_LeftyPitcherOpponent2TileMap: INCBIN "img/play/lefty_pitcher_opponent2.tilemap"
DEF _LEFTY_PITCHER_OPPONENT3_ROWS EQU 7
DEF _LEFTY_PITCHER_OPPONENT3_COLUMNS EQU 8
_LeftyPitcherOpponent3TileMap: INCBIN "img/play/lefty_pitcher_opponent3.tilemap"
ENDC
