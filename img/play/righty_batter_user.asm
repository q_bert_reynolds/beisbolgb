IF !DEF(_RIGHTY_BATTER_USER_TILE_COUNT)
DEF _RIGHTY_BATTER_USER_TILE_COUNT EQU 51
_RightyBatterUserTiles: INCBIN "img/play/righty_batter_user.tiles"
DEF _RIGHTY_BATTER_USER0_ROWS EQU 7
DEF _RIGHTY_BATTER_USER0_COLUMNS EQU 8
_RightyBatterUser0TileMap: INCBIN "img/play/righty_batter_user0.tilemap"
DEF _RIGHTY_BATTER_USER1_ROWS EQU 7
DEF _RIGHTY_BATTER_USER1_COLUMNS EQU 8
_RightyBatterUser1TileMap: INCBIN "img/play/righty_batter_user1.tilemap"
DEF _RIGHTY_BATTER_USER2_ROWS EQU 7
DEF _RIGHTY_BATTER_USER2_COLUMNS EQU 8
_RightyBatterUser2TileMap: INCBIN "img/play/righty_batter_user2.tilemap"
ENDC
