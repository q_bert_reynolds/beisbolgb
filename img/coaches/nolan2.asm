IF !DEF(_NOLAN2_TILE_COUNT)
DEF _NOLAN2_TILE_COUNT EQU 33
DEF _NOLAN2_ROWS EQU 7
DEF _NOLAN2_COLUMNS EQU 7
_Nolan2Tiles: INCBIN "img/coaches/nolan2.tiles"
_Nolan2TileMap: INCBIN "img/coaches/nolan2.tilemap"
ENDC
