IF !DEF(_NOLAN1_TILE_COUNT)
DEF _NOLAN1_TILE_COUNT EQU 32
DEF _NOLAN1_ROWS EQU 7
DEF _NOLAN1_COLUMNS EQU 7
_Nolan1Tiles: INCBIN "img/coaches/nolan1.tiles"
_Nolan1TileMap: INCBIN "img/coaches/nolan1.tilemap"
ENDC
