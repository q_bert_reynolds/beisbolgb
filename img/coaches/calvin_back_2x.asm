IF !DEF(_CALVIN_BACK_2X_TILE_COUNT)
DEF _CALVIN_BACK_2X_TILE_COUNT EQU 16
DEF _CALVIN_BACK_2X_ROWS EQU 8
DEF _CALVIN_BACK_2X_COLUMNS EQU 8
_CalvinBack2xTiles: INCBIN "img/coaches/calvin_back_2x.tiles"
_CalvinBack2xTileMap: INCBIN "img/coaches/calvin_back_2x.tilemap"
ENDC
