SECTION "Play Ball Images", ROMX, BANK[PLAY_BALL_BANK]

INCLUDE "img/baseball.asm"
INCLUDE "img/circle.asm"
INCLUDE "img/strike_zone.asm"

INCLUDE "img/play/lefty_batter_user.asm"
INCLUDE "img/play/lefty_batter_opponent.asm"
INCLUDE "img/play/lefty_pitcher_user.asm"
INCLUDE "img/play/lefty_pitcher_opponent.asm"
INCLUDE "img/play/righty_batter_user.asm"
INCLUDE "img/play/righty_batter_opponent.asm"
INCLUDE "img/play/righty_pitcher_user.asm"
INCLUDE "img/play/righty_pitcher_opponent.asm"
