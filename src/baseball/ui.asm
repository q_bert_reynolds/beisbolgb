
DrawPlayerUI:: ;a = team (0 = user, 1 = opponent)
  push af;team
  ld c, a;team
  and a
  jr z, .userPlayer
.opposingPlayer
  xor a
  ld [_x], a
  ld a, 3
  ld [_y], a;if (team) x = 0, y = 3;
  jr .checkHomeAway
.userPlayer
  ld a, 8
  ld [_x], a
  ld a, 10
  ld [_y], a ;else x = 8, y = 10;
.checkHomeAway
  ld b, 1
  ld a, [home_team]
  cp c
  jr z, .teamIsHome
  xor a
  ld b, a
.teamIsHome
  xor a
  ld [_b], a
  ld a, [frame]
  and a, 1
  cp b
  jr z, .teamIsPitching
  ld a, 1
  ld [_b], a;team is batting
.teamIsPitching ;b = (team == home_team) != (frame % 2);

  ld a, " "
  ld hl, tile_buffer
  ld [hli], a;tiles[0] = " ";
  inc hl

  xor a
  ld [_i], a
.nameBarLoop;for (i = 0; i < 10; i++) 
    ld a, "─"
    ld [hli], a ;tiles[i+2] = "─";
    ld a, [_i]
    inc a
    ld [_i], a
    cp 10
    jr nz, .nameBarLoop

  pop af;team
  push af
  and a
  jr nz, .setOpponentName
  call GetCurrentUserPlayer
  call GetUserPlayerName
  jr .drawName
.setOpponentName
  call GetCurrentOpponentPlayer
  call GetPlayerNumber
  call GetPlayerName
.drawName
  ld hl, name_buffer
  call str_Length;de = len
  ld a, 12
  sub a, e;(12-len)
  srl a;(12-len)/2
  inc a;1+(12-len)/2
  ld b, d
  ld c, e; bc = len
  ld hl, tile_buffer
  ld d, 0
  ld e, a;de = 1+(12-len)/2
  add hl, de
  ld d, h
  ld e, l;de = tile_buffer+1+(12-len)/2
  ld hl, name_buffer
  call mem_Copy

  pop af;team
  push af
  and a
  jr nz, .setOpponentAge
  call GetCurrentUserPlayer
  jr .drawAge
.setOpponentAge
  call GetCurrentOpponentPlayer
.drawAge
  ld d, h
  ld e, l
  ld hl, tile_buffer+12
  call SetAgeTiles
.doneWithAge
  ld a, [_b]
  and a
  jr z, .isPitcher ;if (b) {
  ld hl, tile_buffer+1
  call CurrentOrderInLineup
  add a, "1"
  ld [hl], a ;tiles[1] = "1" + p->batting_order;
  ld hl, tile_buffer+15
  ld a, "<BA>"
  ld [hl], a ;tiles[15] = "<BA>";
  jr .doneWithPlayerStats
.isPitcher ;else {
  ld hl, tile_buffer+1
  ld a, "<P>"
  ld [hl], a ;tiles[1] = p->position;
  ld hl, tile_buffer+15
  ld a, "<ERA>"
  ld [hl], a ;tiles[15] = "<ERA>";

.doneWithPlayerStats
  ld a, " "
  ld hl, tile_buffer+20 ;tiles[20] = " ";
  
  ld a, [_x]
  ld d, a
  ld a, [_y]
  ld e, a
  ld h, 12
  ld l, 2
  ld bc, tile_buffer
  call gbdk_SetBkgTiles;set_bkg_tiles(x,y,12,2,tiles);

  ld a, [_b]
  and a
  jr z, .setERA;if (b) 
.setBatAvg
  pop af;team
  push af
  ld de, 324
  call BattingAvgToString
  ld hl, str_buffer+4
  ld a, " "
  ld [hl], a
  ld a, [_x]
  add a, 4
  ld d, a
  ld a, [_y]
  inc a
  ld e, a
  ld h, 5
  ld l, 1
  ld bc, str_buffer
  call gbdk_SetBkgTiles ;set_bkg_tiles(x+4,y+1,4,1,batting_avg(p));
  jr .setHealthPct

.setERA
  pop af;team
  push af
  ld hl, 902
  call EarnedRunAvgToString
  ld hl, str_buffer+4
  ld a, " "
  ld [hl], a

  ld a, [_x]
  add a, 4
  ld d, a
  ld a, [_y]
  inc a
  ld e, a
  ld h, 5
  ld l, 1
  ld bc, str_buffer
  call gbdk_SetBkgTiles ;else set_bkg_tiles(x+4,y+1,4,1,earned_run_avg(p));

.setHealthPct 
  pop af;team
  ld a, 85
  call HealthPctToString
  ld a, [_x]
  add a, 9
  ld d, a
  ld a, [_y]
  inc a
  ld e, a
  ld h, 3
  ld l, 1
  ld bc, str_buffer
  call gbdk_SetBkgTiles;set_bkg_tiles(x+9,y+1,3,1,health_pct(p));
  ret

DrawBases::
  ld a, " ";for (i = 0; i < 5; i+=2) tiles[i] = " ";
  ld hl, tile_buffer
  ld [hl], a
  ld hl, tile_buffer+2
  ld [hl], a
  ld hl, tile_buffer+4
  ld [hl], a

  ld hl, tile_buffer+5 ;tiles[5] = (runners_on_base & FIRST_BASE_MASK) ? "◆" : "◇";
  ld a, "◇"
  ld [hl], a
  ld a, [runners_on_base+1]
  and a, FIRST_BASE_MASK
  and a
  jr z, .notOnFirst
  ld a, "◆"
  ld [hl], a
.notOnFirst

  ld hl, tile_buffer+1 ;tiles[1] = (runners_on_base & SECOND_BASE_MASK) ? "◆" : "◇";
  ld a, "◇"
  ld [hl], a
  ld a, [runners_on_base+1]
  and a, SECOND_BASE_MASK
  and a
  jr z, .notOnSecond
  ld a, "◆"
  ld [hl], a
.notOnSecond
  
  ld hl, tile_buffer+3 ;tiles[3] = (runners_on_base & THIRD_BASE_MASK) ? "◆" : "◇";
  ld a, "◇"
  ld [hl], a
  ld a, [runners_on_base]
  and a, THIRD_BASE_MASK >> 8
  and a
  jr z, .notOnThird
  ld a, "◆"
  ld [hl], a
.notOnThird

  ld d, 9;x
  ld e, 0;y
  ld h, 3;w
  ld l, 2;h
  ld bc, tile_buffer
  call gbdk_SetBkgTiles;set_bkg_tiles(9,0,3,2,tiles);
  ret

DrawCountOutsInning::
  ld hl, tile_buffer
.setBallsTiles
  ld a, "B"
  ld [hli], a
  xor a
  ld [_i], a
.setBallsLoop
    ld a, "◌"
    ld [hl], a
    call GetBalls
    ld b, a
    ld a, [_i]
    cp b
    jr nc, .skipBall
    ld a, "○"
    ld [hl], a
  .skipBall
    ld a, [_i]
    inc a
    inc hl
    ld [_i], a
    cp 3
    jr nz, .setBallsLoop

.setStrikesTiles
  ld a, "S"
  ld [hli], a
  xor a
  ld [_i], a
.setStrikesLoop
    ld a, "◌"
    ld [hl], a
    call GetStrikes
    ld b, a
    ld a, [_i]
    cp b
    jr nc, .skipStrike
    ld a, "○"
    ld [hl], a
  .skipStrike
    ld a, [_i]
    inc a
    inc hl
    ld [_i], a
    cp 2
    jr nz, .setStrikesLoop
  
.setOutsTiles
  ld a, "O"
  ld [hli], a
  xor a
  ld [_i], a
.setOutsLoop
    ld a, "◌"
    ld [hl], a
    call GetOuts
    ld b, a
    ld a, [_i]
    cp b
    jr nc, .skipOut
    ld a, "○"
    ld [hl], a
  .skipOut
    ld a, [_i]
    inc a
    inc hl
    ld [_i], a
    cp 2
    jr nz, .setOutsLoop
  
.setInningFrameTiles
  ld [hl], "▼"
  ld a, [frame]
  bit 0, a
  jr nz, .skip ;tiles[0] = (frame % 2 == 0) ? "▲" : "▼";
  ld [hl], "▲"
.skip
  inc hl
  ld d, h
  ld e, l
  srl a;frame/2
  inc a
  ld l, a
  xor a
  ld h, a
  call str_Number

.drawCountOutsInning
  ld hl, tile_buffer
  call str_Length
  ld h, e;w
  ld l, 1;h
  ld d, 0;x
  ld e, 2;y
  ld bc, tile_buffer
  call gbdk_SetBkgTiles
  ret

DrawTeamNames::
  ld hl, user_name
  ld de, name_buffer
  ld bc, 8
  call mem_Copy;memcpy(name_buff, user_name, 8);
  
  ld hl, name_buffer
  call str_Length;l = strlen(name_buff);
  ld h, e ;w
  ld l, 1
  ld de, 0;x = y = 0
  ld a, [home_team]
  and a
  jr z, .setPlayerAway
  ld de, 1;x = 0, y = 1  
.setPlayerAway
  ld bc, name_buffer
  call gbdk_SetBkgTiles;set_bkg_tiles(0,y,7,1,name_buff);

  ld a, [game_state]
  and a, GAME_STATE_UNSIGNED_PLAYER
  jr z, .setOpposingTeamName

.setUnsignedPlayerName
  call GetCurrentOpponentPlayer
  call GetPlayerNumber
  call GetPlayerName
  jr .drawOpposingName

.setOpposingTeamName
  ld hl, rival_name;TODO: get coach/team name
  ld de, name_buffer
  ld bc, 8
  call mem_Copy;memcpy(name_buff, rival_name, 8);

.drawOpposingName
  ld hl, name_buffer
  call str_Length;l = strlen(name_buff);
  ld h, e ;w
  ld l, 1
  ld de, 0;x = y = 0
  ld a, [home_team]
  and a
  jr nz, .setOpponentAway
  ld de, 1;x = 0, y = 1  
.setOpponentAway
  ld bc, name_buffer
  call gbdk_SetBkgTiles;set_bkg_tiles(0,1,7,1,name_buff);
  ret

DrawScore::
  xor a
  ld h, a
  ld a, [away_score]
  ld l, a
  ld de, name_buffer
  call str_Number

  ld hl, name_buffer
  call str_Length;l = strlen(name_buff);
  ld h, e ;w
  ld l, 1 ;h
  ld d, 8 ;x
  ld e, 0 ;y
  ld bc, name_buffer
  call gbdk_SetBkgTiles ;set_bkg_tiles(9,0,l,1,name_buff);
  
  xor a
  ld h, a
  ld a, [home_score]
  ld l, a
  ld de, name_buffer
  call str_Number;sprintf(name_buff, "%d", away_score);

  ld hl, name_buffer
  call str_Length;l = strlen(name_buff);

  ld h, e ;w
  ld l, 1 ;h
  ld d, 8 ;x
  ld e, 1 ;y
  ld bc, name_buffer
  call gbdk_SetBkgTiles ;set_bkg_tiles(9,1,l,1,name_buff);
  ret

DrawPlayBallUI::
  call DrawTeamNames
  call DrawScore
  call DrawBases

  ld b, 0
  ld c, 12
  ld d, 20
  ld e, 6
  ld a, DRAW_FLAGS_BKG
  call DrawUIBox

  call DrawCountOutsInning

  ld d, 9
  ld e, 14
  ld h, 10
  ld l, 3
  ld bc, PlayMenuString
  call gbdk_SetBkgTiles;set_bkg_tiles(9,14,10,3,

  DISPLAY_ON
  ret 

MovePlayMenuArrow:
  xor a
  ld [_i], a
.columnLoop ;for (i = 0; i < 2; i++) {
    xor a
    ld [_j], a
  .rowLoop ;for (j = 0; j < 2; j++) {
      ld hl, tile_buffer
      ld a, " "
      ld [hl], a
      ld a, [_x]
      ld b, a
      ld a, [_i]
      cp b
      jr nz, .notArrow
      ld a, [_y]
      ld b, a
      ld a, [_j]
      cp b
      jr nz, .notArrow
      ld a, "▶"
      ld [hl], a
    .notArrow ;tiles[0] = (x==i && y==j) ? "▶" : " ";
      ld a, [_i]
      add a, a;i*2
      add a, a;i*4
      ld b, a
      ld a, [_i]
      add a, a;i*2
      add a, b;i*6
      add a, 8;i*6+8
      ld d, a ;x
      ld a, [_j]
      add a, a;j*2
      add a, 14;j*2+14
      ld e, a;y
      ld a, 1
      ld h, a
      ld l, a
      ld bc, tile_buffer
      call gbdk_SetBkgTiles;set_bkg_tiles(i*6+8,j*2+14,1,1,tiles);

      ld a, [_j]
      inc a
      ld [_j], a
      cp 2
      jr nz, .rowLoop
    ld a, [_i]
    inc a
    ld [_i], a
    cp 2
    jr nz, .columnLoop
  ret

SelectPlayMenuItem::
  ld a, [play_menu_selection]
  and 1
  ld [_x], a ;x = play_menu_selection % 2;

  ld a, [play_menu_selection]
  and 2
  srl a 
  ld [_y], a ;y = (play_menu_selection & 2) >> 1;

  call MovePlayMenuArrow
  WAITPAD_UP
.moveMenuArrowLoop ;while (1) {
    call UpdateInput ;k = joypad();
.testUpPressed ;if (k & J_UP && y == 1) { y = 0; move_play_menu_arrow(); }
    ld a, [button_state]
    and a, PADF_UP
    jr z, .testDownPressed
    ld a, [_y]
    cp 1
    jr nz, .testDownPressed
    xor a
    ld [_y], a
    jr .moveArrow
.testDownPressed ;else if (k & J_DOWN && y == 0) { y = 1; move_play_menu_arrow(); }
    ld a, [button_state]
    and a, PADF_DOWN
    jr z, .testLeftPressed
    ld a, [_y]
    and a
    jr nz, .testLeftPressed
    ld a, 1
    ld [_y], a
    jr .moveArrow    
.testLeftPressed ;else if (k & J_LEFT && x == 1) { x = 0; move_play_menu_arrow(); }
    ld a, [button_state]
    and a, PADF_LEFT
    jr z, .testRightPressed
    ld a, [_x]
    cp 1
    jr nz, .testRightPressed
    xor a
    ld [_x], a
    jr .moveArrow
.testRightPressed ;else if (k & J_RIGHT && x == 0) { x = 1; move_play_menu_arrow(); }
    ld a, [button_state]
    and a, PADF_RIGHT
    jr z, .testAPressed
    ld a, [_x]
    xor a
    jr nz, .testAPressed
    ld a, 1
    ld [_x], a
.moveArrow
    call MovePlayMenuArrow
    
.testAPressed ;if (k & J_A) break;
    ld a, [button_state]
    and a, PADF_A
    jr nz, .exitMoveMenuArrowLoop
    call gbdk_WaitVBL
    jr .moveMenuArrowLoop
.exitMoveMenuArrowLoop
  ld a, [_y]
  add a, a ;y*2
  ld b, a
  ld a, [_x];y*2+x
  add a, b 
  ld [play_menu_selection], a
  ret

MoveMoveMenuArrow:
  xor a
  ld [_i], a
  ld hl, tile_buffer
.setTilesLoop ;for (i = 0; i < 4; i++) {
    ld a, [_i]
    ld b, a
    ld a, [move_choice]
    cp b
    jr z, .setArrow
    ld a, " "
    jr .setTile
.setArrow
    ld a, "▶"
.setTile
    ld [hli], a;tiles[i] = (i == y) ? "▶" : 0;
    ld a, [_i]
    inc a
    ld [_i], a
    cp 4
    jr nz, .setTilesLoop

  ld d, 6
  ld e, 13
  ld h, 1
  ld l, 4
  ld bc, tile_buffer
  call gbdk_SetBkgTiles;set_bkg_tiles(6,13,1,4,tiles);
  ret

ShowMoveInfo::;[_d] = move mask
  ld b, 0
  ld c, 8
  ld d, 11
  ld e, 5
  ld a, DRAW_FLAGS_BKG
  call DrawUIBox

  ld d, 1
  ld e, 9
  ld h, 5
  ld l, 1
  ld bc, TypeSlashText
  call gbdk_SetBkgTiles;set_bkg_tiles(1,9,5,1,"TYPE/");

  call GetCurrentUserPlayer
  push hl;current user player
  ld a, [_d]
  ld d, a
  ld a, [move_choice]
  call GetPlayerMove
  ld a, [move_data.type]
  call GetTypeString

  ld hl, name_buffer
  call str_Length
  ld h, e
  ld l, 1
  ld d, 2
  ld e, 10
  ld bc, name_buffer
  call gbdk_SetBkgTiles;set_bkg_tiles(2,10,strlen(name_buff),1,name_buff);
  
  pop de;current user player
  ld hl, tile_buffer
  ld a, [_d];move mask
  ld b, a
  ld a, [move_choice]
  call SetMovePPTiles;a = move, b = move mask, de = player, hl = tile address
  
  ld h, 5
  ld l, 1
  ld d, 5
  ld e, 11
  ld bc, tile_buffer
  call gbdk_SetBkgTiles
  ret

SelectMoveMenuItem:: ;returns selection in a
  ld d, 0
  ld e, 8
  ld h, 20
  ld l, 10
  ld bc, bkg_buffer
  call gbdk_GetBkgTiles;get_bkg_tiles(0,8,20,10,bkg_buff);

  ld b, 5
  ld c, 12
  ld d, 15
  ld e, 6
  ld a, DRAW_FLAGS_BKG
  call DrawUIBox

.drawMoveNames
  xor a
  ld [_i], a
  call GetCurrentUserPlayer
  push hl
  call IsUserFielding
  jr nz, .userIsFielding
.userIsBatting
  ld d, BATTING_MOVES
  jr .getMoveCount
.userIsFielding
  ld d, PITCHING_MOVES
.getMoveCount
  call GetPlayerMoveCount
  ld [_c], a;move count
  pop hl;player
.loopMoves
    push hl;player
    push de;move mask
    ld a, [_i]

    call GetPlayerMoveName

    ld hl, name_buffer
    call str_Length

    ld h, e
    ld l, 1
    ld d, 7
    ld a, [_i]
    add a, 13
    ld e, a
    
    ld bc, name_buffer
    call gbdk_SetBkgTiles

    pop de;move mask
    pop hl;player
    ld a, [_i]
    inc a
    ld [_i], a
    cp MAX_MOVES
    jr nz, .loopMoves

  ld a, d;move mask
  ld [_d], a
  call MoveMoveMenuArrow
  call ShowMoveInfo

  WAITPAD_UP;update_waitpadup();
.loop ;while (1) {
    call gbdk_WaitVBL
    call UpdateInput;  k = joypad();
.checkButtonUp;  if (k & J_UP && move_choice > 0) {
    ld a, [button_state]
    and PADF_UP
    jr z, .checkButtonDown
    ld a, [move_choice]
    and a
    jr z, .checkButtonDown
    dec a ;--move_choice;
    ld [move_choice], a
    call MoveMoveMenuArrow
    call ShowMoveInfo
    WAITPAD_UP
    jp .loop

.checkButtonDown;  else if (k & J_DOWN && move_choice < c-1) {
    ld a, [button_state]
    and PADF_DOWN
    jr z, .checkButtonStartA
    ld a, [_c];move count
    dec a
    ld c, a
    ld a, [move_choice]
    cp c
    jr nc, .checkButtonStartA
    inc a
    ld [move_choice], a
    call MoveMoveMenuArrow
    call ShowMoveInfo
    WAITPAD_UP
    jp .loop

.checkButtonStartA;  if (k & (J_START | J_A)) {
    ld a, [button_state]
    and PADF_START | PADF_A
    jp z, .checkButtonB
    ld a, [move_choice]
    push af
    jr .exit

.checkButtonB;  else if (k & J_B) break;
    ld a, [button_state]
    and PADF_B
    jp z, .loop
    ld a, -1
    push af

.exit
  call gbdk_WaitVBL
  ld d, 0
  ld e, 8
  ld h, 20
  ld l, 10
  ld bc, bkg_buffer
  call gbdk_SetBkgTiles;set_bkg_tiles(0,8,20,10,bkg_buff);

  pop af
  inc a
  ret
