NEWCHARMAP baseball

;SDCC converts unicode to ascii for you and breaks the charmap
;As a workaround translate.py replaces unicode chars with <strings>: 
charmap "<dots>", 10
charmap "<ball>", 11
charmap "<arrow left>", 12
charmap "<arrow right>", 13
charmap "<arrow right off>", 14
charmap "<top left>", 17
charmap "<top right>", 18
charmap "<bottom left>", 19
charmap "<bottom right>", 20
charmap "<_>", 21
charmap "<|>", 22
charmap "<base empty>", 23
charmap "<base occupied>", 24
charmap "<K>", 27
charmap "<down arrow>", 28
charmap "<up arrow>", 29
charmap "<return>", 30
charmap "<+>", 31
charmap "<i`>", 91
charmap "<u:>", 93
charmap "<u`>", 94
charmap "<o`>", 95
charmap "<a`>", 96
charmap "<!>", 123
charmap "<?>", 125
charmap "<n~>", 126
charmap "<e`>", 127

charmap "<P>", 1 ; pitcher
charmap "<C>", 2 ; catcher
charmap "<1B>", 3 ; first
charmap "<2B>", 4 ; second
charmap "<3B>", 5 ; third
charmap "<SS>", 6 ; short
charmap "<LF>", 7 ; left
charmap "<CF>", 8 ; center
charmap "<RF>", 9 ; right
charmap "◌", 10 ; dotted circle
charmap "○", 11 ; baseball
charmap "◀", 12
charmap "▶", 13
charmap "▷", 14
charmap "<to>", 15
charmap "<yr>", 16
charmap "┌", 17
charmap "┐", 18
charmap "└", 19
charmap "┘", 20
charmap "─", 21
charmap "│", 22
charmap "◇", 23
charmap "◆", 24
charmap "<ERA>", 25
charmap "<BA>", 26
charmap "ꓘ", 27
charmap "▼", 28 ;bottom of the inning
charmap "▲", 29 ;top of the inning
charmap "↵", 30 ;end/return
charmap "┼", 31
charmap "í", 91
charmap "ü", 93
charmap "ú", 94
charmap "ó", 95
charmap "á", 96
charmap "¡", 123
charmap "¿", 125
charmap "ñ", 126
charmap "é", 127
charmap "\n", 128

; Standard printable ASCII characters that RGBDS 0.8 no longer provides for us
charmap " ", 32
charmap "!", 33
charmap "\"", 34
charmap "#", 35
charmap "$", 36
charmap "%", 37
charmap "&", 38
charmap "'", 39
charmap "(", 40
charmap ")", 41
charmap "*", 42
charmap "+", 43
charmap ",", 44
charmap "-", 45
charmap ".", 46
charmap "/", 47
charmap "0", 48
charmap "1", 49
charmap "2", 50
charmap "3", 51
charmap "4", 52
charmap "5", 53
charmap "6", 54
charmap "7", 55
charmap "8", 56
charmap "9", 57
charmap ":", 58
charmap ";", 59
charmap "<", 60
charmap "=", 61
charmap ">", 62
charmap "?", 63
charmap "@", 64
charmap "A", 65
charmap "B", 66
charmap "C", 67
charmap "D", 68
charmap "E", 69
charmap "F", 70
charmap "G", 71
charmap "H", 72
charmap "I", 73
charmap "J", 74
charmap "K", 75
charmap "L", 76
charmap "M", 77
charmap "N", 78
charmap "O", 79
charmap "P", 80
charmap "Q", 81
charmap "R", 82
charmap "S", 83
charmap "T", 84
charmap "U", 85
charmap "V", 86
charmap "W", 87
charmap "X", 88
charmap "Y", 89
charmap "Z", 90
charmap "[", 91
charmap "\\", 92
charmap "]", 93
charmap "^", 94
charmap "_", 95
charmap "`", 96
charmap "a", 97
charmap "b", 98
charmap "c", 99
charmap "d", 100
charmap "e", 101
charmap "f", 102
charmap "g", 103
charmap "h", 104
charmap "i", 105
charmap "j", 106
charmap "k", 107
charmap "l", 108
charmap "m", 109
charmap "n", 110
charmap "o", 111
charmap "p", 112
charmap "q", 113
charmap "r", 114
charmap "s", 115
charmap "t", 116
charmap "u", 117
charmap "v", 118
charmap "w", 119
charmap "x", 120
charmap "y", 121
charmap "z", 122
charmap "\{", 123
charmap "|", 124
charmap "\}", 125
charmap "~", 126
